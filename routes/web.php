<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/winners','WinnerController@mshindi')->name('winners');

Route::get('/custom-register','CustomAuthController@showRegisterForm')->name('custom.register');
Route::post('/custom-register','CustomAuthController@register')->name('custom.register');

Route::get('/custom-login','CustomAuthController@showLoginForm')->name('custom.login');
Route::get('/login','CustomAuthController@showLoginPrompt')->name('login');
Route::post('/custom-login','CustomAuthController@login')->name('custom.login');

Route::get('/forgot_password','CustomAuthController@showRecoverForm')->name('forgot_password');
Route::post('/recover_password','CustomAuthController@recoverPassword')->name('recover_password');


Route::get('/reset_password/{phone_no}','CustomAuthController@showResetPasswordForm')->name('reset_password');
Route::post('/reset_password/{phone_no}','CustomAuthController@resetPassword')->name('reset_password');

Route::get('logout', 'CustomAuthController@logout');

Route::get('/play', function () {
    return view('play');
})->name('play');

Route::post('/play', 'ProfileController@play')->name('play');

Route::get('/play/{pick1?}',
    'EntriesTableController@pick'
    )->name('play.pick');

Route::get('/request_deposit','RequestDepositController@showDepositForm')->name('request_deposit');
Route::post('/request_deposit','RequestDepositController@requestDepo')->name('request_deposit');

Route::get('/withdraw','ProfileController@showWithdrawForm')->name('withdraw');
Route::post('/withdraw','ProfileController@withdraw')->name('withdraw');

Route::get('/ticket_history', 'ProfileController@tickets')->name('tickets');
