<?php
namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateProfile
{
public function handle($request, Closure $next)
{
//If request does not comes from logged in employer
//then he shall be redirected to  Login page
if (!Auth::guard('profile')->check()) {
    return redirect('/custom-login');
}
return $next($request);
}
}