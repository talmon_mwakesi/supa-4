<?php

namespace App\Http\Controllers;

use App\InboxTable;
use Illuminate\Http\Request;

class InboxTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InboxTable  $inboxTable
     * @return \Illuminate\Http\Response
     */
    public function show(InboxTable $inboxTable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InboxTable  $inboxTable
     * @return \Illuminate\Http\Response
     */
    public function edit(InboxTable $inboxTable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InboxTable  $inboxTable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InboxTable $inboxTable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InboxTable  $inboxTable
     * @return \Illuminate\Http\Response
     */
    public function destroy(InboxTable $inboxTable)
    {
        //
    }
}
