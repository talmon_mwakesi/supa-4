<?php

namespace App\Http\Controllers;

use App\Profile;
use App\RequestDeposit;
use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{

    public function  play(Request $request){


        if(session()->has('profile_id')){

            $messages = [
                'numbers.size' => 'You must pick exactly 4 lucky numbers,not more,not less',
                'numbers.required' => 'You must pick any 4 lucky numbers to play',
                'stake.required' => 'You have to place a minimum stake of Kshs. 20 to play',
                'stake.min' => 'Your stake must be at least Kshs. 20',
            ];

            $this->validate($request, [
                'numbers' => 'required|size:11',
                'stake' => 'required|numeric|min:20'
            ], $messages);


            $client = new \GuzzleHttp\Client();

            $params = [
                'json' =>
                    [
                        'profile_id'=>$request->profile_id,
                        'msisdn' => $request->msisdn,
                        'stake' => $request->stake,
                        'numbers' => $request->numbers
                    ]
            ];
            $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/play', $params);
            $response = $response->getBody()->getContents();


            $message = json_decode($response);

            if(isset($message->message) ){
                $request->session()->put('profile_id', $request->profile_id );
                return redirect('/play')->with('success', $message->message);
            }
            if(isset($message->lowBalance)){
                return redirect('/request_deposit')->with('error', $message->lowBalance);
            }
            if(isset($message->error)){
                return redirect('/play')->with('error', $message->error);
            }


        }

        else {

            if(\Route::currentRouteName()  == 'play'){
                $url = url()->previous();
            }


            return redirect('custom-login')->with('url', $url);
        }



    }



    public function  deposit(Request $request){

     
        $balance = RequestDeposit::where('profile_id', '=', $request->profile_id)->first();
        if ($balance <= 40) {
            return redirect('/request_deposit');
        }


//      dd($request->lucky_numbers);
        $client = new \GuzzleHttp\Client();

//        dd($request->numbers);
        $params = [
            'json' =>
                [
                    'profile_id'=>$request->profile_id,
                    'msisdn' => $request->msisdn,
                    'stake' => $request->stake,
                    'numbers' => $request->numbers
                ]
        ];
        $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/play', $params);
        $response = $response->getBody()->getContents();

        $message = json_decode($response);

        if(isset($message->message) ){
            /*$request->session()->put('profile_id', $message->profile_id );
            $request->session()->put('balance', $message->balance );*/
            return redirect('/play')->with('success', $message->message);
        }

        if(isset($message->error)){
            return redirect('/play')->with('error', $message->error);
        }



    }


    public function showWithdrawForm() {

        return view('withdraw');

    }

    public function  withdraw(Request $request){

        /*  $messages = [
              'lucky_numbers.size' => 'You must pick exactly 4 lucky numbers,not more,not less',
              'lucky_numbers.required' => 'You must pick any 4 lucky numbers to play',
              'stake.required' => 'You have to place a minimum stake of Kshs. 20 to play',
              'stake.min' => 'Your stake must be at least Kshs. 20',
          ];

          $this->validate($request, [
              'lucky_numbers' => 'required',
              'stake' => 'required|numeric|min:20'
          ], $messages);
         */
        $client = new \GuzzleHttp\Client();

        $params = [
            'json' =>
                [
                    'msisdn' => $request->msisdn,
                    'amount' => $request->amount,
                ]
        ];

        $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/withdraw', $params);
        $response = $response->getBody()->getContents();

        $message = json_decode($response);

        if(isset($message->message) ){
            return redirect('/play')->with('success', $message->message);
        }

        if(isset($message->error)){
            return redirect('/play')->with('error', $message->error);
        }

    }

//


    public function  getBalance(){


        $client = new \GuzzleHttp\Client();

        $response = $client->get('http://104.155.17.94/lotto/supa4/api/v1/checkbal');
        $response = $response->getBody()->getContents();

        $message = json_decode($response);

        return $message;

    }


    public function tickets(){

        $msisdn = session()->get('msisdn');
        $client = new \GuzzleHttp\Client();

        $params = [
            'json' =>
                [
                    'msisdn' => $msisdn,
                ]
        ];

        $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/ticket_history', $params);
        $response = $response->getBody()->getContents();

        $message = json_decode($response);

        if(isset($message->data) ){
            $ticket_history = $message->data;

            return view('ticket_history', compact('ticket_history'));
        }

        if(isset($message->error)){
            return redirect('/play')->with('error', $message->error);
        }
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }

    public function getPlayPage(){
        return view('play');
    }
}
