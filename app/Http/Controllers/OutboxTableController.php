<?php

namespace App\Http\Controllers;

use App\OutboxTable;
use Illuminate\Http\Request;

class OutboxTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OutboxTable  $outboxTable
     * @return \Illuminate\Http\Response
     */
    public function show(OutboxTable $outboxTable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OutboxTable  $outboxTable
     * @return \Illuminate\Http\Response
     */
    public function edit(OutboxTable $outboxTable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OutboxTable  $outboxTable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OutboxTable $outboxTable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OutboxTable  $outboxTable
     * @return \Illuminate\Http\Response
     */
    public function destroy(OutboxTable $outboxTable)
    {
        //
    }
}
