<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomAuthController extends Controller
{
    protected function guard()
    {
        return Auth::guard('profile');
    }

    public function showRegisterForm(){
        return view('custom.register');

    }


    public function register(Request $request){

        $messages = [
            'msisdn.required' => 'Your phone number cannot be empty!',
            'msisdn.unique' => 'This phone number is already registered,please choose another one',
            'checked.required' => 'You can only proceed after agreeing to terms on the checkbox'
        ];

        $this->validate($request,[
            'msisdn' => 'required|numeric',
            'checked' => 'required',
            'password' => 'required|confirmed'
        ],$messages);

        $client = new \GuzzleHttp\Client();

        $params = [
            'json' =>
            [
                'msisdn' => $request->msisdn,
                'password' => $request->password
            ]
        ];
        $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/register', $params);
        $response = $response->getBody()->getContents();

        $message = json_decode($response);

        if(isset($message->message)) {
            $request->session()->put('profile_id', $message->profile_id );
            return redirect('/play')->with('success', $message->message);
        }

        if(isset($message->error)){
            return redirect('/custom-register')->with('error', $message->error);
        }
    }


    public function showLoginForm(){
        return view('custom.login');

    }

    public static function  getBalance( $msisdn)
    {
        $params = [
            'json' =>
                [
                    'msisdn' => $msisdn
                ]
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/checkbal', $params);
        $response = $response->getBody()->getContents();
        $message = json_decode($response);

        return $message;
    }


    public function showLoginPrompt(){
        return view('login_prompt');
    }


    public function  login(Request $request){

        $messages = [
            'msisdn.required' => 'Your phone number cannot be empty!',
        ];

        $this->validate($request,[
            'msisdn' => 'required|numeric',
            'password' => 'required'
        ],$messages);

        $client = new \GuzzleHttp\Client();

        $params = [
            'json' =>
                [
                    'msisdn' => $request->msisdn,
                    'password' => $request->password
                ]
        ];

        $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/login', $params);
        $response = $response->getBody()->getContents();

        $message = json_decode($response);


        if(isset($message->message) ){
            $balance = $this->getBalance($message->msisdn);
            $request->session()->put('profile_id', $message->profile_id );
            $request->session()->put('balance', $balance );
            $request->session()->put('msisdn', $message->msisdn );

            if($request->url != null){
                return redirect($request->url)->with('success', $message->message);
            }

            else{
                return redirect('/play')->with('success', $message->message);
            }
        }

        if(isset($message->error)){
            return redirect('/custom-login')->with('error', $message->error);
        }

    }



    public function logout(Request $request) {
        Auth::logout();
        $request->session()->flush();
        return redirect('/');
    }


    public function showRecoverForm(){
        return view('recover_password');
    }

    public function recoverPassword(Request $request){
        $messages = [
            'msisdn.required' => 'Your phone number cannot be empty!',
            'msisdn.numeric' => 'Your phone number must contain only numbers!'
        ];

        $this->validate($request,[
            'msisdn' => 'required|numeric',
        ],$messages);

        $client = new \GuzzleHttp\Client();

        $params = [
            'json' =>
                [
                    'msisdn' => $request->msisdn,
                ]
        ];
        $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/recover', $params);
        $response = $response->getBody()->getContents();

        $message = json_decode($response);


        if(isset($message->data->message) ){
            $phone_no = $request->msisdn;
            return redirect('/reset_password/'.$phone_no)->with('success', $message->data->message);
        }

        if(isset($message->error)){
            return redirect('/custom-login')->with('error', $message->error);
        }


    }

    public function showResetPasswordForm($phone_no){
        return view('reset_password', compact('phone_no'));
    }

    public function resetPassword(Request $request, $phone_no){
        $messages = [
            'msisdn.required' => 'Your phone number cannot be empty!',
            'msisdn.numeric' => 'Your phone number must contain only numbers!'
        ];

        $this->validate($request,[
            'msisdn' => 'required|numeric',
            'code' => 'required|numeric',
            'password' => 'required|confirmed',

        ],$messages);

        $client = new \GuzzleHttp\Client();

        $params = [
            'json' =>
                [
                    'msisdn' => $request->msisdn,
                    'code' => $request->code,
                    'password' => $request->password,
                ]
        ];


        $response = $client->post('http://104.155.17.94/lotto/supa4/api/v1/reset', $params);

        $response = $response->getBody()->getContents();

        $message = json_decode($response);



        if(isset($message->message)){

            return redirect('/custom-login')->with('success', $message->message);
        }
        else{
            return redirect()->route('reset_password')->with('error', $message->error);
        }


    }
}
