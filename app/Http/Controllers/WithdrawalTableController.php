<?php

namespace App\Http\Controllers;

use App\WithdrawalTable;
use Illuminate\Http\Request;

class WithdrawalTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WithdrawalTable  $withdrawalTable
     * @return \Illuminate\Http\Response
     */
    public function show(WithdrawalTable $withdrawalTable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WithdrawalTable  $withdrawalTable
     * @return \Illuminate\Http\Response
     */
    public function edit(WithdrawalTable $withdrawalTable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WithdrawalTable  $withdrawalTable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WithdrawalTable $withdrawalTable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WithdrawalTable  $withdrawalTable
     * @return \Illuminate\Http\Response
     */
    public function destroy(WithdrawalTable $withdrawalTable)
    {
        //
    }
}
