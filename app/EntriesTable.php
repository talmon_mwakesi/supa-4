<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntriesTable extends Model
{
    protected $table = 'entries';

    protected $primaryKey = 'entry_id';

    protected $fillable = [ 'profile_id','lucky_numbers', 'stake','ticket_number','source','play_status'  ];

    public function profile(){
        return $this->belongsTo('App\Profile');
    }
}
