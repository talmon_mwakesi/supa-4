<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDeposit extends Model
{
    protected  $table = 'request_deposit';

    public function profile(){
        return $this->belongsTo('App\Profile');
    }
}
