<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Profile extends Authenticatable
{
    protected $table = 'profile';

    protected $primaryKey = 'profile_id';

    protected $fillable = ['msisdn', 'password'];

    public function entries(){
        return $this->hasMany('App\EntriesTable');
    }

    public function requestdeposits(){
        return $this->hasMany('App\RequestDeposit');
    }


}
