<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'withdrawal';

    /**
     * Run the migrations.
     * @table withdrawal
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('withdrawal_id');
            $table->integer('inbox_id');
            $table->string('msisdn', 25);
            $table->string('raw_text', 200);
            $table->decimal('amount', 64, 2);
            $table->string('reference', 45)->nullable()->default(null);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->string('created_by', 45);
            $table->enum('status', ['PROCESSING', 'QUEUED', 'SUCCESS', 'FAILED', 'TRX_SUCCESS', 'CANCELLED', 'SENT', 'UNKOWN', 'REVERSED'])->nullable()->default(null);
            $table->string('provider_reference', 250)->nullable()->default(null);
            $table->integer('number_of_sends')->default('0');
            $table->float('charge')->default('30');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
