<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'deposit';

    /**
     * Run the migrations.
     * @table deposit
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('deposit_id');
            $table->string('reference_id', 55);
            $table->string('status', 55);
            $table->string('description')->nullable()->default(null);
            $table->string('name', 55);
            $table->double('amount');
            $table->string('msisdn', 15);
            $table->string('pg_trx_id', 20)->nullable()->default(null);
            $table->integer('paybill_no')->nullable()->default(null);
            $table->integer('profile_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
