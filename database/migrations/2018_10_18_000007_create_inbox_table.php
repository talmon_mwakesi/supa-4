<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'inbox';

    /**
     * Run the migrations.
     * @table inbox
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('inbox_id');
            $table->string('msisdn', 15);
            $table->double('balance')->nullable()->default(null);
            $table->string('text', 160);
            $table->integer('shortcode')->nullable()->default(null);
            $table->string('network', 120)->nullable()->default(null);
            $table->string('link_id', 120)->nullable()->default(null);
            $table->string('response')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
