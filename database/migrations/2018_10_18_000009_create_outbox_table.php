<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutboxTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'outbox';

    /**
     * Run the migrations.
     * @table outbox
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('outbox_id');
            $table->string('msisdn', 15);
            $table->string('profile_id', 11)->nullable()->default(null);
            $table->string('text')->nullable()->default(null);
            $table->string('status', 55);
            $table->integer('shortcode')->nullable()->default(null);

            $table->index(["profile_id"], 'profile_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
