<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinnerTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'winner';

    /**
     * Run the migrations.
     * @table winner
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('winner_id');
            $table->enum('winner_type', ['AIRTIME', 'PRIZE']);
            $table->integer('entry_id');
            $table->integer('odd')->default('0');
            $table->double('stake');
            $table->double('win_amount');
            $table->string('winning_lucky_no', 11);
            $table->enum('status', ['PENDING', 'SUCCESSFUL', 'REVERSED', 'FAILED', 'PAID']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
