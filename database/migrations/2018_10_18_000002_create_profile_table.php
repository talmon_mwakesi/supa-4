<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'profile';

    /**
     * Run the migrations.
     * @table profile
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('profile_id');
            $table->string('msisdn', 15);
            $table->string('source', 55)->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('network_name', 35)->nullable()->default(null);
            $table->integer('status')->nullable()->default('1');
            $table->string('password')->nullable()->default(null);
            $table->string('created_by', 100)->nullable()->default(null);

            $table->index(["msisdn"], 'msisdn');

            $table->index(["created_at"], 'created_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
