<?php
require_once 'net/net.php';
session_start();
//
if(!isset($_SESSION['isLoggedIn'])) {
header("Location: login.php");
}
$net =  new Net();
$profile_id = isset($_SESSION['profile'])?$_SESSION['profile']:null;
if(is_null($profile_id)){
header('Location: login.php');
}
$ans = "";
if(isset($_POST['play'])){
$ans = $_POST['question_answer'];
}
// die(var_dump($_POST, 1));
$response = $net->play($profile_id, $ans);
// die(var_export($response, 1));
$response_code  = $response['code'];
$question = null;
switch($response_code){
case 100:
$message = $response['message'];
break;
case 200:
$message = $response['message'];
$question_array = preg_split("/\d{1}\)/", $response['question']);
$question = $question_array[0];
$response_A = $question_array[1];
$response_B = $question_array[2];
break;
default:
break;
}
?>
<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>inuka Lotto - Play</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- Add your site or application content here -->
  <div class="site-wrap">
   <div class="header">
     <div class="container">
       <div class="row">
         <div class="col-xs-4">
           <a href="index.php" class="logo">
             <img src="imgs/logo.jpg" alt="">
           </a>
         </div>
         <div class="col-xs-8 text-right">
           <ul class="menu nav navbar-nav">
              <li><a href="logout.php">log out</a></li>
              <li><a href="pay.php">Deposit</a></li>
           </ul>
           <div class="acc">
            <span><?php echo $response['name']; ?> | </span>
            <span> <span id="total-points">Points:
                <b><?php echo $response['points']; ?>  </b> </span> | Bal: <b id="bal"><?php echo $response['balance']; ?> </b></span>
           </div>
         </div>
       </div>
     </div>
   </div>
    <div class="pool">
      <div class="container">
        <div class="row">
        <div class="col-xs-12">
      <?php if($response_code == 200 ) { ?>
      <h4 class="headline">Answered Questions (<?php
      echo $response['question_count']; ?>) </h4>
      
        <?php echo $message; ?>
 
      <form id="quiz-form" action="" method="post">
        <div id="questions">
          <section>
            <h3> <?php echo
            $question ?></h3>
            <div class="single-ans">
              <input type="radio"
              name="question_answer"
              id="question-1-answers-A"
              value="1" />
              <label
                for="question-1-answers-A">A)
                <?php echo $response_A;
              ?></label>
            </div>
            
            <div class="single-ans">
              <input type="radio"
              name="question_answer"
              id="question-1-answers-B"
              value="2" />
              <label
                for="question-1-answers-B">B)
              <?php echo $response_B; ?></label>
            </div>
            <!-- <a id="answerbutton" class="checkanswer btn btn-success" href="">Submit Answer</a> -->
            <div class="single-ans" >
              <input class = "btn btn-play btn-lg
              btn-block "
              name="play" type="submit"
              value="SUBMIT ANSWER" />
            </div>
            
          </section>
        </div>
      </form>
      <?php } else { ?>
      <h4> <?php echo $message; ?> </h4>
      <?php } ?>
    </div> 
    </div>       
        </div>
      </div>
    <div class="section">
         <div class="winnings">
    <h4>You have </h4>
     <h3><span class="winners"><?php echo $response['points']; ?></span> points, in  <span class="money"><?php echo $response['nextDraw']; ?>00Hrs  draw</span> </h3>
     <h4>NEXT DRAW IN <span class="timer"> <?php echo $response['mins']; ?>  MIN</span></h4>
     <h4>THE MORE YOU PLAY THE HIGHER YOUR CHANCES OF WINNING</h4>
   </div>
   <div class="howtoplay">
     <div class="container">
       <div class="row">
         <div class="col-sm-10 col-offset-sm-1">
          <!-- <h3 title="how to play"><img src="imgs/how.png" width=200 alt="how to play"></h3> -->
            <ol>
              <li>Deposit as little as 10 ksh to paybill 290055.</li>
              <li>Each question you answer earns you entry points to that hourly draw. Every question costs 10 ksh.</li>
              <li>The more entries you have the higher your chances of winning. </li>
            </ol>
         </div>
       </div>
     </div>
   </div>
   <div class="footer">
     <div class="container">
       <div class="row">
              <div class="col-md-12 text-center footnote">
                    <p>*By sending the word "Inuka" to 29155, you authorize inuka lotto to send messages to the registered number. Consent is not required as a condition of purchase, Your text is your electronic signature agreeing to these terms and giving electronic writen consent, contact inuka lotto for a free paper of these terms, Msg and data rates may apply. Not all carriers covered. Play responsibly. Must be 18 or older to play.</p>  
                        <p class="">Copyright &copy; inuka lotto. All rights reserved. By participating in inuka Lotto you agree to the <a href="terms.php"> TERMS AND CONDITIONS</a></p>
                    </div>
                </div>
       </div>
     </div>
    </div>
    <script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <!-- <script src="js/jquery.steps.js"></script> -->
    <script src="js/main.js"></script>
  </body>
</html>
