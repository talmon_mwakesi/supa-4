<?php
require_once '../Logger.php';
require_once 'Core.php';
require_once '../wallet.php';
require_once '../utilities/fdUtilities.php';
class Routes extends Core{
    public function _construct($request,$origin){
        parent::_construct($request);
        $this->log = new Logger ("/var/log/lotto/lottoFD_info.log");
    }
    /*
     *       Example of an Endpoint
     */

    protected function example($args,$pdo) {
        if ($this->method == 'GET') {
            return "TBLS:" . print_r($args);
        } else {
            return "Only accepts GET requests";
        }
    }
    protected function play(){
        $this->log->LogDebug("Received Play Request --->".print_r($this->file,true));
        $playdata = json_decode($this->file,true);
        extract($playdata);
        $msisdn =$this->validatemsisdn($msisdn);
        if(!isset($msisdn) || !isset($profileID)||!isset($stake)||!isset($numbers)){
            return array("resultCode"=>0,"error"=>"Some fields are not set");
        }
        $profileiD = $this->getProfileId($msisdn);

        if($profileiD !==$profileID){
            return array("resultCode"=>0,"error"=>"Invalid Request.");
        }

        if( (int)$stake >5000|| (int)$stake <10){
            return array("resultCode"=>0,"error"=>"Invalid stake. Maximum stake is Ksh.5000 and Minimum stake is Ksh.10.");
        }
        //let's separate numbers & aletters
        $numbers = (int) preg_replace('/[^0-9]/', '', $this->message);
        $playamount = $this->lotto_config['play_amount'];
        $this->log->LogDebug($this->msisdn." NUMBERS PICKED: ".$numbers);
        if(strlen($numbers) > 8 || $numbers <10 || $numbers > 99 || empty($numbers) ){
            return array("resultCode"=>0,"error"=>"Wrong lucky numbers Format.");
        }
        $walletDebitResponse = $this->debit($msisdn, $stake,$this->lotto_config['pin'],$this->lotto_config['source']);

        if(!$walletDebitResponse['success']){
            return array("resultCode"=>0,"error"=>"You do not have enough credit to play. Your current balance is Ksh.".
                    $walletDebitResponse['balance'].". Kindly deposit to continue playing.");
        }
        $data = [
            "profile_id"=>$this->profileiD,
            "lucky_numbers"=>$this->number,
            "inbox_id"=>$this->inbox_id,
            "stake"=>$this->stake,
            "source"=>"SF_API_KEYWD",
            "play_status"=>"AWAITING RESPONSE"
                ];
        $entryID=$this->utils->create_lotto_entry($data);
        if($entryID < 1){
            return array("resultCode"=>0,"error"=>"We are experiencing technical Problem. Kindly try again later.");
        }
        return array(
                'resultCode'=>1,
                'message'=>"Congratulations! You have succesfully entered the DOSIKA Draw  and now stand a chane to WIN BIG ",
                );
    }
    private function getOdd($entry_id){
        $query="select * from entries e where entry_id=".$entry_id ." AND play_status='PRIZE'";
        $res = $this->db->select($query);
        return $res[0];
    }

    public function send_mo_message($response_message){
        $this->log->LogDebug("Link ID set sending direct response ==> "
                . $response_message );
        $sql_message = $this->db->escape($response_message);
        $this->outbox($sql_message);
        $this->send_sms($sql_message, $this->outbox_id, $this->link_id,
                $this->msisdn,$this->shortcode,$this->sdp_id,$this->mo_config);

    }
    protected function register(){
        if($this->method !='POST'){
            return array("resultCode"=>0,'error'=>'UNKNOWN HTTP METHOD');
        }

        if(!isset($this->file) || empty($this->file)){
            return array("resultCode"=>0,'error'=>'NO DATA RECEIVED');
        }

        $data = json_decode($this->file,true);
        //Check that relevant data has been submitted


        //Validate MSISDN
        if((null === $msisdn=$data['msisdn']) or
                empty($msisdn=$data['msisdn']) or
                !$this->validatemsisdn($data['msisdn'])){

            return array("resultCode"=>0,'error'=>"The phone number is incorrect");
        }
        $msisdn = $this->validatemsisdn($data['msisdn']);
        //Validate password
        if(!isset($data['password']) or empty($data['password'])){
            return array("resultCode"=>0,"error"=>"Kindly check your Password");
        }
        /*
           Check if user already exists
         *If user deposited before registration their profile is created but without Password
         *So update their profile, inserting password, status and update_at where msisdn is their phone_no.
         *If user registered first, then they exist, ask them to login or change password if forgotten
         *
         */
        $searchNo ="select p.profile_id from (select profile_id, msisdn from profile) as p inner join (select profile_id from web_user) as w on p.profile_id = w.profile_id where p.msisdn=$msisdn";
        $isExist=$this->db->select($searchNo);
        $this->log->LogDebug("some complicated query to check if user exists----->~".print_r($isExist,TRUE));
        if(!empty($isExist) ){
            return array("resultCode"=>0,'error'=>'User Exists, Kindly try reseting your Password');
        }

        //Let's Hash the password
        $hashedPassword = $this->wtfPassword($data['password']);
        $userStatus="select status from profile where msisdn= $msisdn";
        $isStatus=$this->db->select($userStatus);
        $this->log->LogDebug("****************************".$isStatus[0]['status']);
        if(($isStatus[0]['status'] == '0') || ($isStatus[0]['status'] > '0')){
            //if($isStatus[0]['user_status'] == '0'){
            $updateProfile="update profile set password='".$hashedPassword."' , status=1, updated_at=now() where msisdn=$msisdn";
            $isUpdated=$this->db->update($updateProfile);
            $this->log->LogDebug("User Existed so we just update the password---->");
            $balance =$this->get_balance($msisdn,$this->lotto_config['pin'],$this->lotto_config['source']);
            return array(
                    "resultCode"=>1,
                    "msisdn"=>$msisdn,
                    "profile_id"=>$profile_id,
                    "balance"=>$balance["balance"],
                    "message"=>"Registration Succesful !"
                    );

        }else{
            //Create user profile
            $hashedPassword = $this->utils->password_hash($data['password']);
            $profileQ = "insert into profile set msisdn =$msisdn,"
                ."status=1,"."password='".$hashedPassword."',created_at=now(),updated_at=now()";
            $profile_id=$this->db->query($profileQ,1);
        }
        if($profile_id < 1){
            return array(
                    "resultCode"=>0,
                    "error"=>"We are unable to create your account."
                    ."Kindly contact system Admin"
                    );
        }
        $ActivationCode = $this->utils->Acode();
        //Create WebUser Account
        $registerWebUser = $this->registerweb($profile_id,$hashedPassword,$ActivationCode);
        if($registerWebUser['created']){
            //The user has been created
            $ret = array('resultCode'=>0,'message'=>"Login",'code'=>200,'profileID'=>$profile_id);
            $this->log->LogDebug("$this->origin | API | New user created successfuly on Web_User, msisdn: $msisdn ");
            //SEND WELCOME SMS
            $this->log->LogDebug("-> Sending Registration Message <-");
            $response_message ="Registration Successful! \n
                Welcome to DOSIKA. To PLAY, deposit a minimum of Ksh.".$this->lotto_config['deposit_amount']
                ."Then select 4 LUCKY Numbers to stand a chance to WIN BIG!!.
                Play More to WIN more. \n
                For any inquiry call Customer Care on 0729290224";
            $outboxID =  $this->outbox($msisdn, $response_message, $this->lotto_config['bulk_code'],$profile_id);
            $this->send_sms($response_message, $outboxID,'1', $msisdn,
                    $this->lotto_config['bulk_code'],$this->lotto_config['bulk_service_id'],$this->get_mo_config());
        }else{
            $ret = array('resultCode'=>0,'message'=>"The account exists, We've sent you an sms. Use the code to login.",'code'=>100);
            $sms = "We have created your account on PesaWheel, kindly use $ActivationCode to activate your account.";
            //send the sms
            $outboxID =  $this->outbox($msisdn, $sms, $this->lotto_config['bulk_code'],$profile_id);
            $this->send_sms($sms, $outboxID,'1', $msisdn,$this->lotto_config['bulk_code'],$this->lotto_config['bulk_service_id'],$this->get_mo_config());

        }
        $balance =$this->get_balance($msisdn,$this->lotto_config['pin'],$this->lotto_config['source']);
        return array(
                "resultCode"=>1,
                "msisdn"=>$msisdn,
                "message"=>"User created successfully",
                "balance"=>$balance["balance"],
                "profile_id"=>$profile_id,
                );

        if(!empty($this->link_id) && !is_null($this->link_id)){
            $this->send_mo_message($response_message);
            return $response_message;

        }
        }
        /*
         *Register a new user on web
         */

        public function registerweb($profile_id,$password,$code){
            $insert = "insert into web_user set profile_id=$profile_id,status=2,verify_code='$code',created=now(),modified=now()";
            if(is_null($this->if_user_exist($profile_id))){
                $this->db->query($insert);
                return ['created'=>1];
            }else{
                return ['created'=>0];
            }
        }
        protected function withdraw(){
            if($this->method !='POST'){
                return array('resultCode'=>0,'error'=>'UNKNOWN HTTP METHOD');
            }

            if(!isset($this->file) || empty($this->file)){
                return array('resultCode'=>0,'error'=>'NO DATA RECEIVED');
            }

            $data = json_decode($this->file,true);
            //Check that relevant data has been submitted

            //Validate MSISDN
            if((null === $msisdn=$data['msisdn']) or
                    empty($msisdn=$data['msisdn']) or
                    !$this->validatemsisdn($data['msisdn'])){
                return array('resultCode'=>0,'error'=>"The phone number is incorrect");
            }
            $msisdn = $this->validatemsisdn($data['msisdn']);

            //validate amount
            if((null === $amount=$data['amount']) or
                    empty($amount=$data['amount'])or
                    ($amount=$data['amount'] <  $this->lotto_config['min_withdraw'])or
                    ($amount=$data['amount'] >  $this->lotto_config['max_withdraw'])){
                return array('resultCode'=>0,'error'=>"Invalid Withdrawal Amount.");
            }
            $amount=$data['amount'];
            $balance =$this->get_balance(
                    $msisdn,
                    $this->lotto_config['pin'],
                    $this->lotto_config['source']
                    );
            $balanceAmount = $balance['balance'];
            $charge = ($amount >= 1000? 22 :15);
            if(($amount+$charge) >=$balanceAmount){
                return array(
                        'resultCode'=>0,
                        'error'=>"You do not have enough balance to complete this transaction.".
                        " Your current balance is Ksh $balanceAmount");
            }
            //Insert into withdraw table
            $wdata=[
                "inbox_id"=>$this->inbox_id,
                "msisdn"=>$this->msisdn,
                "amount"=>$amount,
                "message"=>'withdraw#'.$amount,
                "app"=>'spin-API',
                "charge"=>$charge,
                ];
            $insertID=$this->utils->insert_withdrawal($wdata);
            $withRes =$this->Mainwithdraw(
                    $msisdn,
                    $amount,
                    $this->lotto_config['source'],
                    $this->lotto_config['pin']
                    );

            $updateWithdraw = "update withdrawal set reference='".$withRes['reference'].
                "', charge=".$withRes['charge'].",status='SENT' where withdrawal_id=$insertID";
            $this->db->query($updateWithdraw);

            return array(
                    'resultCode'=>1,
                    'message'=>"We are processing your request to withdraw Ksh $amount.",
                    "balance"=>$withRes['balance'],
                    );
        }
        public function deposit(){

            if($this->method !='POST'){
                return array('resultCode'=>0,'error'=>'UNKNOWN HTTP METHOD');
            }

            if(!isset($this->file) || empty($this->file)){
                return array('resultCode'=>0,'error'=>'NO DATA RECEIVED');
            }

            $data = json_decode($this->file,true);
            //Check that relevant data has been submitted

            //Validate MSISDN
            if((null === $msisdn=$data['msisdn']) or
                    empty($msisdn=$data['msisdn']) or
                    !$this->validatemsisdn($data['msisdn'])){
                return array('resultCode'=>0,'error'=>"The phone number is incorrect");
            }
            $msisdn = $this->validatemsisdn($data['msisdn']);

            //validate amount
            if((null === $amount=$data['amount']) or
                    empty($amount=$data['amount'])){
                return array('resultCode'=>0,'error'=>"Invalid Deposit Amount.");
            }
            //$amount=$data['amount'];
            if($this->requestdepo($msisdn,$amount,$this->lotto_config['source'],rand(1111,9999))){
                if(($amount >= $this->lotto_config['deposit_amount'])) {
                    return array(
                            'resultCode'=>1,
                            'message'=>'Kindly check your phone to complete this transaction.',
                            );
                }else{
                    return array(
                            'resultCode'=>0,
                            'error'=>'Deposit amount is below Minimum.',
                            );
                }
            }
            return array(
                    'resultCode'=>0,
                    'error'=>"We are unable to complete this transaction.Kindly contact the system Admin",
                    );
            $balance =$this->get_balance($msisdn,$this->lotto_config['pin'],$this->lotto_config['source']);
        }
        protected function login(){
            $this->log->LogDebug("Received login Request --->");
            if($this->method !='POST'){
                return array('resultCode'=>0,'error'=>'UNKNOWN HTTP METHOD');
            }

            if(!isset($this->file) || empty($this->file)){
                return array('resultCode'=>0,'error'=>'NO DATA RECEIVED');
            }

            $data = json_decode($this->file,true);
            //Check that relevant data has been submitted

            //Validate MSISDN
            if((null === $msisdn=$data['msisdn']) or
                    empty($msisdn=$data['msisdn']) or
                    !$this->validatemsisdn($data['msisdn'])){

                return array('resultCode'=>0,'error'=>"The phone number is incorrect");
            }
            $msisdn = $this->validatemsisdn($data['msisdn']);
            //Validate password
            if(!isset($data['password']) or empty($data['password'])){
                return array('resultCode'=>0,"error"=>"Kindly check your Password");
            }
            //Check if user already exists
            $searchNo ="select profile_id,password from profile where msisdn= ".$msisdn;
            //Let's Hash the password
            $hashedPassword = $this->utils->passwordhash($data['password']);
            $isExist=$this->db->select($searchNo);
            if(!isset($isExist[0]['profile_id'])){
                return array('resultCode'=>0,'error'=>"User doesn't exist. Kindly Register");
            }
            if($isExist[0]['password'] !==$hashedPassword){
                return array('resultCode'=>0,"error"=>"Wrong Credentials");
            }
            $balance =$this->get_balance(
                    $msisdn,
                    $this->lotto_config['pin'],
                    $this->lotto_config['source']
                    );
            return array(
                    "resultCode"=>1,
                    "message"=>'Authentication Succesful',
                    "profileID"=>$isExist[0]['profile_id'],
                    'status'=>'Authenticated',
                    "balance"=>$balance['balance'],
                    "msisdn"=>$msisdn,
                    );
        }
        public function validate_msisdn($msisdn){
            if(strlen($msisdn) > 12){
                return false;
            }
            $regex = '/(?:0|\+?254)?(7\d{8})/';
            preg_match($regex, $msisdn, $matches);
            if(empty($matches) || sizeof($matches) < 2){
                return false;
            }
            return '254'.$matches[1];
        }
        public function get_profile($msisdn){
            if(empty($msisdn)){
                return null;
            }
            $ql  = "select profile_id from profile where msisdn = '$msisdn'";
            $profile = $this->db->select($ql);
            if(empty($ql)){
                return null;
            }else{
                $this->log->LogDebug("get_profile~select query results~".print_r($profile,TRUE));
                return $profile[0]['profile_id'];
            }
        }
        /*
           CHeck if the account exists on web_user table.

         */
        public function if_user_exist($profile_id){
            if(empty($profile_id)){
                return null;
            }
            $sql="select web_user_id from web_user where profile_id =$profile_id";
            $sqlData = $this->db->select($sql);
            if(empty($sqlData)){
                return null;

            }else{
                return $sqlData[0]['web_user_id'];
            }
        }
        public function recover_update($profile_id,$code){
            $check="select web_user_id from web_user where profile_id=$profile_id";
            $checkuser=$this->db->select($check);
            if(empty($checkuser)){
                $insertwebuser="insert into web_user set profile_id=$profile_id, status=1, verify_code='$code',created=now(),modified=now()";
                if($this->db->query($insertwebuser)>0){
                    return ['created'=>1];
                }else{
                    return['created'=>0];
                }
            }else{
                $update = "update web_user set verify_code ='$code',status=1  where  profile_id=$profile_id";
                if( $this->db->query($update) > 0){
                    return ['created'=>1];
                }else{
                    return ['created'=>0];
                }
            }
        }
        protected function recover(){
            //Make sure the request is a POST else reject and terminate execution.
            if($this->method != 'POST'){
                return array('error'=>'UNKNOWN HTTP METHOD');
            }
            //Check if there is anydata sent
            if(!isset($this->file) || empty($this->file)){
                return array("error"=>"NO DATA RECEIVED");
            }
            $requestData =json_decode($this->file,true);

            //Check if all the params are set
            if(!isset($requestData['msisdn']) ) {
                return array('data'=>['resultCode'=>1,'code'=>'0002'],'code'=>500);
            }
            //Validate the MSISDN
            $msisdn = $this->validate_msisdn($requestData['msisdn']);
            if(is_null($msisdn)){
                return array('data'=>['resultCode'=>1,'code'=>'0003'],'code'=>500);
            }
            $this->log->LogDebug("Fetching the Profile Data==>");
            $profileData = $this->get_profile($msisdn);
            $profile_id=$profileData;
            $this->log->LogDebug("Profile ID==>".$profile_id);
            $ActivationCode = $this->Acode();
            $registerData = $this->recover_update($profile_id,$ActivationCode);
            if($registerData['created']){
                $message = "Kindly use $ActivationCode to reactivate your account.";
                $ret = array('resultCode'=>1,'message'=>"We've sent you an activation code on sms.",'code'=>200);
                $this->log->LogDebug("$this->origin | API | Account recovered msisdn: $msisdn ");
                //send the sms
                $outboxID =  $this->utils->outbox($msisdn, $message, $this->lotto_config['bulk_code'],$profile_id);
                $this->utils->send_sms($message, $outboxID,'1', $msisdn,
                        $this->lotto_config['bulk_code'],$this->lotto_config['bulk_service_id'],$this->get_mo_config());

            }else {
                $ret = array('resultCode'=>0,'message'=>"Account does not exist.",'code'=>100);
            }
            $toUser = array(
                    'data'=>$ret,
                    'code'=>200
                    );
            return $toUser;

        }
        //UI calls this endpoint on reload for Balance
        protected function checkbal(){
            //Check if the request is a POST
            if($this->method != 'POST'){
                return array('error'=>'UNKNOWN HTTP METHOD');
            }
            //Check if there is any data sent
            if(!isset($this->file)||empty($this->file)){
                return array('error'=>"NO DATA RECEIVED");
            }
            $requestData = json_decode($this->file,true);

            //Check if Params are set
            if(!isset($requestData['msisdn'] )){
                return array('data'=>['resultCode'=>1,'code'=>'0002'],'code'=>500);
            }else{
                $msisdn=$requestData['msisdn'];
                $balance = $this->get_balance($msisdn,$this->lotto_config['pin'],$this->lotto_config['source']);
                $this->log->LogDebug("Fetching Balance:===>".$balance);
                $userbal=$balance['balance'];
                $this->log->LogDebug("Found Balance:===>".$userbal);
                //$userbal=$balance['balance'];
                //$updatebal="update outbox set balance = '$userbal' where msisdn = $msisdn  order by outbox_id desc limit 1";
                //$this->db->query($updatebal);
                //$this->log->LogDebug("Updating Balance ===============>$userbal for user::$msisdn");

            }
            return $userbal;
        }
        protected function reset(){
            //Check if the request is a POST
            if($this->method != 'POST'){
                return array('error'=>'UNKNOWN HTTP METHOD');

            }
            //Check if there is any data sent
            if(!isset($this->file)||empty($this->file)){
                return array('error'=>"NO DATA RECEIVED");
            }
            $requestData = json_decode($this->file,true);

            //Check if Params are set
            if(!isset($requestData['newPassword'] ) || !isset($requestData['code']) || !isset($requestData['msisdn']) ){
                return array('data'=>['resultCode'=>1,'code'=>'0002'],'code'=>500);

            }else{
                $vCode = $requestData['code'];
                $this->log->LogDebug("Received password and Code, Proceeding to Validate....");
                $msisdn = $this->validate_msisdn($requestData['msisdn']);
                $passwordHash = $this->utils->password_hash($requestData['newPassword']);
                //Get user Profile ID
                $profileData = $this->get_profile($msisdn);
                $profile_id=$profileData;
                $codequery="select verify_code from web_user where profile_id =". $profile_id;
                $res = $this->db->select($codequery);
                $actCode=$res[0]['verify_code'];
                $this->log->LogDebug("Validate with this Code==>".$actCode);
                if($vCode != $actCode){
                    return array('resultCode'=>0,'error'=>"Kindly Enter Correct Code",'code'=>100);
                }
                $update = "update profile set password = '$passwordHash' where msisdn = $msisdn ";
                $this->db->query($update);
                $retrn = array('resultCode'=>1,'message'=>"Password Updated Succesfully",'code'=>100);
                $Usernotif = array(
                        'data'=>$retrn,
                        'code'=>200
                        );
            }
            return $retrn;
        }


    }

    ?>
