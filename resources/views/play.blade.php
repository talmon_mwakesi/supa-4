@extends('layouts.app')
@section('content')

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="home-banner">
                            <form action="play" method="post">
                                @csrf
                                <ul class="numbers">
                                    @php
                                        $destArray1 = array();
                                        if(isset($_GET['picks'])) {
                                            $destArray1=unserialize($_GET['picks']);
                                        }
                                        for($i = 1; $i <= 48; $i++) {
                                        if ($i < 10){
                                        $i=sprintf( "%d%d" , 0, $i );
                                         }
                                         $class=(in_array($i,$destArray1))? 'active btn btn-radio' : '' ;
                                         $array=$destArray1;
                                         if(count($array) < 4) {
                                            array_push($array, $i);
                                         }
                                         $picks=urlencode(serialize($array));
                                         echo "<li class='box'>" ;
                                         echo "<a href='?picks=$picks' class='$class btn btn-radio'>
                                               $i
                                               </a><br />\n";
                                         echo "</li>" ;
                                         }
                                         $results=array(); array_walk_recursive( $destArray1,
                                        function ($value, $key) use (&$results) { $results[]=$value; });
                                        $numbers=implode(',', $results);
                                    @endphp
                                </ul>

                                <div class="row lucky-numbers">
                                        <div class="col-xs-12">
                                            <p class="text-white">Your Lucky Numbers</p>
                                            <div class="picked">
                                                    <span class="pick1">
                                                        <?php if (isset($results) && $results != null) { echo substr($numbers,0,2); } else { echo "--"; } ?></span>
                                                    <span class="pick2">
                                                        <?php if (isset($results) && $results != null) { echo substr($numbers,3,2); } else { echo "--"; } ?></span>
                                                    <span class="pick3">
                                                        <?php if (isset($results) && $results != null) { echo substr($numbers,6,2); } else { echo "--"; } ?></span>
                                                    <span class="pick4">
                                                        <?php if (isset($results) && $results != null) { echo substr($numbers,9,2); } else { echo "--"; } ?></span>

                                                    <a href="/play" class="btn btn-danger">Clear</a>
                                            </div>
                                        </div>

                            <div class="col-xs-12 stake-plc text-white">
                                                <b>Stake Ksh 20</b>
                                                <i class="text-gold">OR</i>
                                            <label class="customlabel">
                                              <input type="checkbox" name="bonus"> Use Bonus
                                              <div class="checkmark"></div>
                                            </label>
                            
                                            <input type="text" name="numbers" value="{{ implode(',', $results)  }}"
                                                class="hidden">
                                            <input type="text" name="profile_id" value="{{ session('profile_id')  }}"
                                                class="hidden">
                                            <input type="text" name="msisdn" value="{{ session('msisdn')  }}" class="hidden">
                                            <input type="text" name="stake" value="20" class="hidden">
                        </div>  
                        </div>


                        
                        <div class="row ">
                            <div class="col-xs-12 text-center">
                                <button type="submit" class="btn btn-lg play-btn btn-warning">Play Now</button>
                            </div>

                        </div>
                        </form>

                        @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                        <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                        @endif

                    </div>
        </div>
    </div>
</div>
</div>





@stop

