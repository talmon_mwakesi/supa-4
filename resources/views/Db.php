<?php 
/**
 * Database wrapper for a MySQL with PHP tutorial
 * 
 */

class Db {
    // The database connection
    protected static $connection;
    protected $log;
    protected $conf;

    public function __construct($log=NULL, $conf=null) {
        $this->log = $log; //new Logger ( "/var/log/lotto/lotto_db.log" , Logger::DEBUG );
        $this->conf = $conf;
    }
    /**
     * Connect to the database
     * 
     * @return bool false on failure / mysqli MySQLi object instance on success
     */
    public function connect() {
        $config = $this->conf;
        // Try and connect to the database
        if(!isset(self::$connection)) {
            // Load configuration as an array. Use the actual location of your configuration file
            // Put the configuration file outside of the document root
            if(empty($config) || is_null($config)){
                
                $_config = parse_ini_file('/var/www/html/lotto/config.ini');
                $config = $_config['database'];
            }
            self::$connection = new mysqli($config['host'],$config['username'],$config['password'],$config['dbname']);
        }

        // If connection was not successful, handle the error
        if(self::$connection === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
            $this->log->LogDebug("Could not create connection returning FALSE => ". mysqli_connect_error() );
            return false;
        }
        $this->log->LogDebug("DB Connection established: ");
        return self::$connection;
    }

    /**
     * Query the database
     *
     * @param $query The query string
     * @return mixed The result of the mysqli::query() function
     */
    public function query($query, $update=false) {
        $this->log->LogDebug("EXCEUTING QUERY: ". $query);
        // Connect to the database
        $connection = $this -> connect();
        // Query the database
        if(!$result = $connection -> query($query)){
            $this->log->LogDebug("SQL ERROR: ". $this->error());
        }

        if($update){ 
            $this->log->LogDebug("QUERY Insert_id $connection->insert_id");
            return $connection->insert_id == 0 ? 
                $connection->affected_rows: $connection->insert_id; 

        }

        return $result;
    }





    public function update($query) {
        $this->log->LogDebug("EXCEUTING QUERY: ". $query);
        // Connect to the database
        $connection = $this -> connect();

        // Query the database
        if(!$result = $connection -> query($query)){
            $this->log->LogDebug("SQL ERROR: ".mysqli_error($connection));
        }

            $this->log->LogDebug("QUERY rows affected $connection->affected_rows");
            return mysqli_affected_rows($connection);

    }




    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function select($query) {
        $rows = array();
        $result = $this -> query($query);
        if($result === false) {
            $this->log->LogDebug("SELECT RESULT FALSE: ". $this->error());
            return false;
        }
        while ($row = $result -> fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    public function escape($str){
        $connection = $this->connect();
        return $connection->escape_string($str);    
    }

    /**
     * Fetch the last error from the database
     * 
     * @return string Database error message
     */
    public function error() {
        $connection = $this -> connect();
        return $connection -> error;
    }

    /**
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */
    public function quote($value) {
        $connection = $this -> connect();
        return "'" . $connection -> real_escape_string($value) . "'";
    }
}
