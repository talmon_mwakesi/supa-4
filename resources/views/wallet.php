<?php

class Wallet{
    protected $wallet_endpoint;
    protected $log;

    public function __construct($logger, $wallet_conf){
        $this->wallet_endpoint = $wallet_conf['url'];
        $this->wallet_conf = $wallet_conf;
        $this->log = $logger;
        $this->log->LogDebug("Starting wallet broker ..");
    }

    public function get_balance($msisdn,$pin=''){
        if(empty($pin)){
            $pin = $this->wallet_conf["pin"];
        }
        $payload = array(
                "account"=>$msisdn,
                "pin"=>$pin,
                "request"=>"balance",
                );
        $data_string = json_encode($payload);

        $this->log->LogDebug("Prepared Balance equiry: ". $data_string);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Balance enquiry response: ".print_r($response,TRUE));
        $source = $this->wallet_conf["source"];
        if($response['status'] ==200){
            $balance = $response['more'][$source]['accountBalance'];
        }else{
            $balance = null;    
        }
        $arr_balance = is_null($balance) ?
            array(
                    'balance' => NULL,
                    'message' => $response['message'],
                    'status' => $response['status'],
                    'reason' => $response['reason']
                 )
            :   array(
                    'balance' => $balance,
                    'message' => $response['message'],
                    'status' => $response['status']
                    );
        return $arr_balance;
    }

    function qna_get_balance($msisdn,$pin='',$source){
        if(empty($pin)){
            $pin = $this->wallet_conf["pin"];
        }
        $payload = array(
                "account"=>$msisdn,
                "pin"=>$pin,
                "request"=>"balance",
                );
        $data_string = json_encode($payload);
        $this->wallet_endpoint = $this->wallet_conf["url"];
        $this->log->LogDebug("Prepared Balance equiry: ". $data_string);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Balance enquiry response: ".print_r($response,TRUE));
        if($response['status'] ==200){
            $balance = $response['more'][$source]['accountBalance'];
        }else{
            $balance = null;    
        }
        $this->log->LogDebug("Balance is $balance AND SOURCE IS : $source");
        $arr_balance = is_null($balance) ?
            array(
                    'balance' => NULL,
                    'message' => $response['message'],
                    'status' => $response['status'],
                    'reason' => $response['reason']
                 )
            :   array(
                    'balance' => $balance,
                    'message' => $response['message'],
                    'status' => $response['status']
                    );
        return $arr_balance;
    }



    public function runCredit($msisdn, $amount, $source, $names ){
        $payload = array(
                "pin"=>$this->wallet_conf["pin"],
                "uniqueID"=>"LOTTO".date('YmdHis'),
                "amount"=>$amount,      
                "source"=>$source,
                "destination"=>$msisdn,
                "first_name"=>$names,
                "lastname"=>"",
                "request"=>"credit",
                );
        $data_string = json_encode($payload);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Credit response: ".print_r($response,true));
        $credit_success = $response['status'] == 200 && $response['message'] == 'OK';
        return array("balance"=>$response['more'][$source]['accountBalance'],  "success"=>$credit_success);


    }




    public function credit($msisdn, $amount, $source, $names ){
        $payload = array(
                "pin"=>$this->wallet_conf["pin"],
                "uniqueID"=>"LOTTO".date('YmdHis'),
                "amount"=>$amount,  
                "source"=>$source,
                "destination"=>$msisdn,
                "first_name"=>$names,
                "lastname"=>"",
                "request"=>"DEPOSIT",
                );
        $data_string = json_encode($payload);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Credit response: ".var_export($response));
        $credit_success = $response['status'] == 200 && $response['message'] == 'OK';
        return array("balance"=>$response['more'][$source]['accountBalance'],  "success"=>$credit_success);


    }
    public function debit($msisdn, $amount,$pin = "",$source = ""){

        if(empty($pin)){
            $pin = $this->wallet_conf['pin'];
        }

        if(empty($source)){
            $source = $this->wallet_conf['source'];
        }
        $this->log->LogDebug("DEBIT REQUEST: msisdn-> ".$msisdn." amount->".$amount." pin-> ".$pin." source-> ".$source);
        $payload = array(
                "amount"=>$amount,   
                "source"=>$source,
                "destination"=>$msisdn,
                "pin"=>$pin,
                "type"=>"DEBIT",
                "request"=>"withdrawal",
                );
        $data_string = json_encode($payload);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Debit response: ".var_export($response, true));
        $debit_success = $response['status'] == 200 && $response['message'] == 'OK';
        $source = $this->wallet_conf['source'];
        $balance = array_key_exists($source, $response['more'])
            ? $response['more'][$source]['accountBalance']:0;

        return array(
                "balance"=>$balance,
                "success"=>$debit_success,
                "fresponse"=>$response);
    }

    public function withdraw($msisdn, $amount){
        $payload = array(
                "amount"=>$amount,   
                "source"=>$this->wallet_conf['source'],
                "destination"=>$msisdn,
                "pin"=>$this->wallet_conf['pin'],
                "type"=>"b2c",//debit/b2c
                "request"=>"withdrawal",
                );
        $data_string = json_encode($payload);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Credit response: ".var_export($response, true));
        if(is_null($response)){
            return array("balance"=>null,  "success"=>null);
        }
        $withdraw_success = $response['status'] == 200 && $response['message'] == 'OK';
        $source = $this->wallet_conf['source'];
        return array("balance"=>$response['more'][$source]['accountBalance'],  
                "success"=>$withdraw_success,  
                "charge"=>$response['more'][$source]['accountCharge'],
                "reference"=>$response['more'][$source]['referenceNumber']);

    }

    public function xdwithdraw($msisdn, $amount){
        $payload = array(
                "amount"=>$amount,
                "source"=>$this->wallet_conf['source'],
                "destination"=>$msisdn,
                "pin"=>$this->wallet_conf['pin'],
                "type"=>"b2c",//debit/b2c
                "request"=>"withdrawal",
                );
        $data_string = json_encode($payload);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Credit response: ".var_export($response, true));
        if(is_null($response)){
            return array("balance"=>null,  "success"=>null);
        }
        $withdraw_success = $response['status'] == 200 && $response['message'] == 'OK';
        $source = $this->wallet_conf['source'];
        return array("balance"=>$response['more'][$source]['accountBalance'],
                "success"=>$withdraw_success,
                "charge"=>$response['more'][$source]['accountCharge'],
                "reference"=>$response['more'][$source]['referenceNumber']);

    }

    public function qnawithdraw($msisdn, $amount){
        $payload = array(
                "amount"=>$amount,
                "source"=>$this->wallet_conf['qnasource'],
                "destination"=>$msisdn,
                "pin"=>$this->wallet_conf['pin'],
                "type"=>"b2c",//debit/b2c
                "request"=>"withdrawal",
                );
        $data_string = json_encode($payload);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Credit response: ".var_export($response, true));
        if(is_null($response)){
            return array("balance"=>null,  "success"=>null);
        }
        $withdraw_success = $response['status'] == 200 && $response['message'] == 'OK';
        $source = $this->wallet_conf['qnasource'];
        return array("balance"=>$response['more'][$source]['accountBalance'],
                "success"=>$withdraw_success,
                "charge"=>$response['more'][$source]['accountCharge'],
                "reference"=>$response['more'][$source]['referenceNumber']);

    }
    public function qnacredit($msisdn, $amount,$names ){
        $payload = array(
                "pin"=>$this->wallet_conf['pin'],
                "uniqueID"=>"QNA".date('YmdHis'),
                "amount"=>$amount,  
                "source"=>$this->wallet_conf['qnasource'],
                "destination"=>$msisdn,
                "request"=>"CREDIT",
                );
        $data_string = json_encode($payload,TRUE); 

        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug(" RADIO Credit response: ".print_r($response,TRUE));
        $credit_success = $response['status'] == 200 && $response['message'] == 'OK';
        return array("balance"=>$response['more'][$source]['accountBalance'],  "success"=>$credit_success);


    }


    public function post_to_server($params){
        $service_url = $this->wallet_endpoint;
        $curl = curl_init($service_url);
        $curl_post_data = $params;
        $this->log->LogDebug("Curl request:".$service_url.",  ".var_export($params, true));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return json_decode($curl_response, true);
    }


}
?>
