@extends('layouts.app')


@section('content')

<h3 class="text-gold">Tickets History</h3>
<table class="table table-responsive table-striped bg-light">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Ticket No</th>
        <th scope="col">Lucky Numbers</th>
        <th scope="col">Date</th>
    </tr>
    </thead>
    <tbody>

    @php $counter = 1;  @endphp
    @foreach($ticket_history as $ticket)
    <tr>
        <th scope="row">{{ $counter }}</th>
        <td>{{ $ticket->Ticket_No }}</td>
        <td>{{ $ticket->lucky_numbers }}</td>
        <td>{{ $ticket->Date }}</td>
    </tr>
    @php  $counter += 1;  @endphp
    @endforeach
    </tbody>
</table>


@stop