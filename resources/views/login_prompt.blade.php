@extends('layouts.app')


@section('content')

    <p class="lead text-white">You must be logged in to continue.</p>
    <p><a href="/custom-login" style="color: orange; font-size: 28px;">Login here </a> </p>
    <p class="lead text-white">Don't have an account? <a href="/custom-register" style="color: orange; font-size: 28px;">Register Here</a> </p>

@stop