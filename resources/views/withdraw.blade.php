@extends('layouts.app')


@section('content')

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h4 class="heading text-center">Withdraw</h4>
                <form action="{{  route('withdraw') }}" class="login-form" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="msisdn">Phone Number</label>
                        <input type="text" class="form-control" id="msisdn" name="msisdn" >
                    </div>
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="number" class="form-control" id="amount" name="amount" >
                    </div>
                    <button type="submit" class="btn btn-block btn-success">Withdraw</button>
                </form>

                @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <p class="alert alert-danger">{{$error}}</p>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>










@stop