@extends('layouts.app')


@section('content')

    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="heading text-center">Recover Password</h4>
                    <form action="{{  route('reset_password', ['msisdn' =>  $phone_no]) }}" class="login-form" method="post">
                        @csrf
                        <input type="hidden" class="form-control" id="msisdn" value="{{ $phone_no }}"  name="msisdn" >
                        <div class="form-group">
                            <label for="code">Code</label>
                            <input type="text" class="form-control" id="code" name="code" >
                        </div>
                        <div class="form-group">
                            <label for="password">New Password</label>
                            <input type="password" class="form-control" id="password" name="password" >
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm New Password</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" >
                        </div>
                        <button type="submit" class="btn btn-block btn-success">Reset</button>
                    </form>

                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop    