<?php
class fDUtils{
	const CONF_FILE = '/var/www/html/lotto/config.ini';

	public $log;
	public $db;
	public $config=array();
	public $app = 'fd';
	public $dimension;
	public $current_draw;

	public function __construct($log, $dimension='F', $app='FD'){
		$this->log = $log;
		$this->app = $app;
		$this->dimension = $dimension;
		$this->set_config();
		$this->set_lotto_config();
		$this->set_mo_config();
		$this->set_bulk_config();
		$this->init_db();
	}
	/**
	 * initialize db class
	 **/
	private function init_db(){
		$this->db = new Db($this->log,$this->config['supa4database']);
	} 

	/**
	 * initialize config class
	 */
	private function set_config(){
		$this->config  = parse_ini_file($this::CONF_FILE, true);
	}

	/**
	 * Pick and set relevant lotto_config based on dimension
	 */
	private function set_lotto_config(){
		$this->lotto_config = $this->config['LottoFD'];
	}
	/**
	 * Pick and set relevant mo_config based on dimension
	 */
	private function set_mo_config(){
		$key = "mo".$this->dimension."D";
		if(array_key_exists($key, $this->config)){
			$this->mo_config  = $this->config[$key];
		}else{
			$this->mo_config = $this->config['mo'];
		}
	}

	/**
	 * Pick and set relevant bulk_config based on dimension
	 */
	private function set_bulk_config(){
		$key = "bulk".$this->dimension."D";
		if(array_key_exists($key, $this->config)){
			$this->bulk_config  = $this->config[$key];
		}else{
			$this->bulk_config = $this->config['bulk'];
		}
	}

	public function get_lotto_config(){
		return $this->lotto_config;
	}

	public function get_bulk_config(){
		return $this->bulk_config;
	}

	public function get_mo_config(){
		return $this->mo_config;
	}

	public function validate($n, $message){
		return preg_match('/^\d{' . $n
				. '}\s*(?:(?:[\s#*]+\d{1,3}?)(?:[\s#*]+\d\s*)?)?$/', $message);
	}
	public function validate_msisdn($msisdn){
		preg_match('/^(\+)?(254|0)?(7\d{8})\s*/', $msisdn, $matches);
		if(!empty($matches)){
			return "254" . $matches[3];
		}
		return null;
	}
	/**
	 * This function uses bcrypt php lib
	 */
	public function password_hash($password){
		$options = [
			'cost' => 10,
			'salt' => '$2y$11$q5MkhSBtlsJcNEVsYh64a.aCluzHnGog7'
				. 'TQAKVmQwO9C8xb.t89F.',
			];
		return password_hash($password, PASSWORD_BCRYPT, $options);
	}
	protected function withdrawback(){
		/*
		   What is expected:
		   {'status': u'COMPLETE', 'msisdn': u'254740579427', 'receipt_number': u'MGN0NZV8ZI', 'balance': u'40.00', 'reference': u'EWALLET_5b55e6fca7c39'}
		 */
		parse_str($this->file, $requestData);
		$this->log->LogDebug(" withdrawBC $this->origin  API:: [ $this->request ]
				DATA:[".json_encode($requestData)." ]");

		$msisdn = $requestData['msisdn'];
		$ref = $requestData['reference'];
		$status = $requestData['status'];
		if($status=='COMPLETE'){
			$dbstatus='SUCCESS';
			$this->log->LogDebug("Withdrawal_STATUS:: ".$dbstatus);
		}else{

			$dbstatus='FAILED';
			$this->log->LogDebug("Withdrawal_STATUS:: ".$dbstatus);
		}
		$this->utils->withdrawStatus($msisdn,$ref,$dbstatus);
		$data = ["status"=>1,"desc"=>"Record updated"];
		return ['data'=>$data,"code"=>200];

	}
	//Update withdrawal status
	public function withdrawStatus($msisdn,$ref,$status){
		$query = "update withdrawal set status = '$status' where msisdn = $msisdn and  reference='$ref'";
		return  $this->db->query($query, 1);
	}

	public function deposit(
			$profile_id,$names,$amount,$paybill,$msisdn,$refno,$trxid){
		$insertq = "insert into deposit set msisdn =".
			"$msisdn,profile_id=$profile_id,amount=$amount,".
			"reference_id='$refno',paybill_no='$paybill',status='RECEIVED',".
			"pg_trx_id='$trxid',name='$names',created_at=now(),updated_at=now()";
		$this->db->query($insertq);
	}
	public function updateName($profileID,$name){
		$update = "update profile set name = '$name',network_name='SAFARICOM' where profile_id = $profileID";
		if( $this->db->query($update) > 0){
			return true;
		}else{
			return false;
		}

	}
	/*
	   Generate Password and Account Activation CODE

	 */
	public function Acode(){
		$randomCode = rand(1,9) . rand(0,9) . rand(1,9) . rand(0,9);
		return $randomCode;
	}

	public function winners(){
		$query = "select
			w.updated_at,e.stake,('won')win_amount,left(p.msisdn,8)msisdn,e.lucky_numbers from
			winner w inner join entries e using (entry_id) inner join profile p
			using(profile_id) where w.win_amount > 0 order by w.winner_id desc limit 20;";
		$res = $this->db->select($query);
		return $res;
	}
	//Generates ticket number from entry number and updates the exact entry on entry table
	public function create_ticket($msisdn){
		$query = " select e.entry_id eid, e.profile_id pid from entries e inner join profile p on e.profile_id=p.profile_id where p.msisdn = $msisdn order by e.created_at desc limit 1;";
		$queryRes=$this->db->select($query);
		$eid=$queryRes['0']['eid'];
		$pid=$queryRes['0']['pid'];
		$hexed_id = strtoupper(dechex($eid));
		$ticket_no = 'D05KA'.$hexed_id;
		 $this->log->LogDebug("EID ==>> $eid PID ==> $pid");
		if(!$eid){
			return "";
		}
		$update_q = "update entries set ticket_number = '$ticket_no' where " .
			"entry_id = '$eid' order by created_at desc limit 1";
		$update = $this->db->query($update_q);
		return $ticket_no;

	}
	public function getLinkID($inboxID){
		$query = "select link_id from inbox where inbox_id =$inboxID";
		$dbRes = $this->db->select($query);
		return $dbRes[0]['link_id'];
	}
	public function updaterd($id,$mpesaCode){
		$query ="update request_deposit set status=1,mpesa_code='$mpesaCode' where request_deposit_id=$id";
		$this->db->query($query);
	}
	public function newrequestDeposit($profileId,$msisdn,$amount){
		$query = "insert into request_deposit set profile_id=$profileId,msisdn=$msisdn,amount=$amount,status=0,created_at=now(),updated_at=now();";
		$insertID = $this->db->query($query,1);
		return $insertID;
	}
	public function toretry(){
		$query = "select e.entry_id,i.msisdn,i.inbox_id from entries e inner join inbox i using (inbox_id)  where e.updated_at < date_sub(now(), interval 1 minute)  and i.response is null and date(e.created_at) = curdate() order by 1 desc;";
		$dbRes = $this->db->select($query);
		$msisdnq=$dbRes['msisdn'];
		$inbox_idq=$dbRes['inbox_id'];
		return $dbRes;
	}
	//get the inbox_id to retry
	public function toretryagain($msisdnq, $inbox_idq){
		$query="select inbox_id from inbox where msisdn=$msisdnq and inbox_id>$inbox_idq";
		$retryInboxId=$this->db->select($query);
		return $retryInboxId;
	}
	public function entriesToday($profileID){
		$query = "select count(e.entry_id) count,p.name from entries e inner join profile p using(profile_id) where e.profile_id = $profileID and date(e.created_at) = curdate()";
		$dbRes = $this->db->select($query);
		return $dbRes[0];
	}
	public function get_random_number($len=4){
		$randomNumbersArray = array_map(function() {
				return mt_rand(0, 99);
				}, range(0,$len-1));
		return join($randomNumbersArray);

	}

	/* -----------------------------------*
	 * just check if profile exists
	 * case scenario for non-prolific users
	 * -----------------------------------*/
	public function get_profile($msisdn)
	{
		$this->log->LogDebug("MSISDN ==>> $msisdn");
		$ql  = "select profile_id, name from profile" .
			"where msisdn = '$msisdn'";
		$profile = $this->db->select($ql);

		$this->log->LogDebug("get__profile ~ select query results ~ "
				. print_r($profile,TRUE));

		if(empty($profile)){
			$this->is_new_profile = true;
		}else{
			$this->is_new_profile = false;
		}
		return  $this->is_new_profile;
	}
	public function get_msisdn($profileId){
		$query = "select msisdn from profile where profile_id = $profileId";
		$msisdn = $this->db->select($query);
		return $msisdn;
	}
	public function get_or_create_profile($msisdn,$createdby,
			$message,$network_name='SAFARICOM'){

		$ql  = "select profile_id, name from profile "
			. " where msisdn = '$msisdn'";

		$profile = $this->db->select($ql);

		$this->log->LogDebug("get_or_create_profile~select query results~"
				. print_r($profile,TRUE));

		//Create profile if missing
		if(empty($profile)){
			$profileQ = "insert into profile (msisdn, created_at, updated_at, " .
				"source, status) values ("
				. $this->db->quote($msisdn). ', '
				. 'now(), now(),'
				. $this->db->quote($createdby) . ', '
				. ' 1)';
			$this->log->LogDebug("Received New Profile creating ...");

			$profile_id = $this->db->query($profileQ, 1);
			$is_new_profile = true;
			$names = null;

		}else{
			$is_new_profile = false;
			$profile_id = $profile[0]['profile_id'];
			$names = $profile[0]['name'];
		}
		return array(
				'profile_id'=>$profile_id,
				'names'=>$names,
				'is_new_profile'=>$is_new_profile
			    );
	}
	/*---------------------------------*
	 *      EXTRACT FIRST NAME ONLY    *
	 * ------------------------------- */
	public function explode_name($name){
		return explode('*', $name)[0];
	}
	/*------------------------------*
	 * GET LUCK DIP FOR USER MESSAGE
	 *------------------------------*/
	public function get_lucky_dip($message){
		$m = explode('*', $message);
		return sizeof($m) > 1? $m[1]:null;

	}

	/*--------------------------*
	 * GET ENTRY FOR USER MESSAGE
	 *--------------------------*/
	public function get_entry($message){
		$m = preg_split('/[\s#*]+/', $message);
		return $m[0];
	}

	/*-----------------------------------------------------*
	 * GENERATE RANDOM $len DIGIT NUMBER BETWEEN 10 AND 99 *
	 *-----------------------------------------------------*/
	public function get_lucky_numbers(){
		return rand(10, 99);
	}
	/*----------------------------------------*
	 * LOG OUTBOX AND UPDATE RESPONSE IN INBOX
	 *----------------------------------------*/

	public function outbox($msisdn, $message, $shortcode,$profile_id, $ref,$balance=0, $inbox_id=null){

		$outboxQ = "insert into outbox (outbox_id,status,text,msisdn,profile_id,"
			. "shortcode,created_at,updated_at)"
			. "values (null, 'SENT',"
			.  $this->db->quote($message) . ", "
			.  $this->db->quote($msisdn) . ", "
			.  $this->db->quote($profile_id) . ","
			.  $this->db->quote($shortcode) . ", "
			.  "now(),now())";

		$outbox_id = $this->db->query($outboxQ, 1);
		if(!is_null($inbox_id)) {
			$bl = number_format( (float) $balance, 2, '.', '');
			$update = "update inbox set balance=$bl,response = '"
				. $this->db->escape($message) . "' where "
				. "inbox_id = '" . $inbox_id . "' limit 1";
			$this->db->query($update);
		}
		return $outbox_id;
	}
	/*---------------------------------------------------------------*
	 * Log entry to inbox and set global params for request procesing
	 *---------------------------------------------------------------*/
	public function inbox($msisdn, $shortcode, $network,$message,$linkID=''){
		$inbox_ql = "insert into inbox ( link_id,network, shortcode, "
			. " msisdn, text,created_at, updated_at "
			. ") values( '$linkID',"
			.  $this->db->quote($network) . ", "
			.  $this->db->quote($shortcode) . ", "
			.  $this->db->quote($msisdn) . ", "
			.  $this->db->quote($message) . ","
			. " now(), now()) ";

		$inbox_id = $this->db->query($inbox_ql, 1);
		return [
			'inbox_id'=>$inbox_id,
			'network'=>$network,
			'shortcode'=>$shortcode,
			];
	}
	/*---------------------------------------------------
	 * CONSTRUCT QUERY, SEND MESSAGE via KANNEL SQL BOX
	 */
	public function send_sms($message, $outbox_id, $link_id, $msisdn,$shortcode,$sdp_id,$mo_config){
		$meta_data = urlencode(
				sprintf("?http?sdp_service_id=%s&correlator=%s&link_id=%s",
					$sdp_id, $outbox_id, $link_id));

		$query =  "INSERT INTO kannel.lotto_send_sms(momt, sender, receiver,"
			. " msgdata, time, smsc_id, sms_type, boxc_id, meta_data) "
			. " VALUES ('mo', '" . $shortcode . "', "
			. " '$msisdn', ".$this->db->quote($message).", '". date('YmdHis')."', "
			. "'". $mo_config['smsc_id'] ."', 2, '".$mo_config['box_id']."', "
			. " '$meta_data') ";
		$this->sql_id = $this->db->query($query, 1);

	}

	public function update_entry($data) {
		$query = "update entries set stake="
			.$this->db->quote($data['stake'] ).",play_status="
			.$this->db->quote($data['play_status'] ).",updated_at = now() "
			." where entry_id=".$data['entry_id'];
		$this->db->query($query);
		return true;
	}


	public function create_lotto_entry($data) {
		$insert = "insert into entries set profile_id="
			.$this->db->quote($data['profile_id'] ).",lucky_numbers="
			.$this->db->quote($data['lucky_numbers'])
			.",inbox_id=".$data['inbox_id'].",ticket_number = 'D05KA' ".",source="
			.$this->db->quote($data['source']).",play_status="
			.$this->db->quote($data['play_status'])
			.",stake= ".$this->db->quote($data['stake'])
			.",created_at=now(),updated_at=now();";
		$entry_id = $this->db->query($insert, 1);
		return $entry_id;
	}

	public function insert_winner($data){
		/*

		 */
		$insertQ = "insert into winner set winner_type='PRIZE',entry_id="
			.$data['entry_id'].", odd=0,stake=".$data['stake']
			.",win_amount=".$data['win_amount'].",status='PAID',created_at=now(), updated_at=now()";
		$winnerID =$this->db->query($insertQ, 1);
		if(empty($winnerID)){
			$this->log>LogDebug("Unable to create Winner Entry, aborting...".$winnerID);
			return false;
		}
		return $winnerID;
	}
	public function insert_withdrawal($wdata){
		$withdrawal_q = "insert into withdrawal set inbox_id =".$wdata['inbox_id'].",msisdn= " .$wdata['msisdn']
			.", amount=".$wdata['amount'].",raw_text=".$wdata['message'].", reference=null, created_at = now(),  created_by = ".$wdata['app'].",status='PROCESSING', " .
			"provider_reference = null, number_of_sends = 1,charge=".$wdata['charge'].")";
		$wRes=$this->db->query($withdrawal_q, 1);
		return $wREs;
	}



}


?>
