<?php
/*
   Author Kaburu
 */

require_once '../Logger.php';
require_once '../db/Db.php';
require_once '../utilities/fdUtilities.php';
abstract class Core{
    /*
     * DB properties.
     * Define all the database configurations.
     * Function loadDB() at the bottom is used to acquire and manage the PDO connection to the mysql server.
     */
    const CONF_FILE = '/var/www/html/lotto/config.ini';
    public $config;
    public $log;
    protected $utils;
    /**
     *      * Property: method
     *      * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';
    /**
     *      * Property: endpoint
     *      * The Model requested in the URI. eg: /files
     */
    protected $endpoint = '';
    /**
     *      * Property: verb
     *           * An optional additional descriptor about the endpoint, used for things that can
     *           * not be handled by the basic methods. eg: /files/process
     */
    protected $verb = '';
    /**
     *      * Property: args
     *      * Any additional URI components after the endpoint and verb have been removed, in our
     *      * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     *      * or /<endpoint>/<arg0>
     */
    protected $args = Array();
    /**
     *      * Property: file
     *      * Stores the input of the PUT request
     */
    protected $file = Null;
    /**
     *  *Property: db
     *  *PDO object connection to the db.
     *
     */
    public $db =Null;

    /**
     *      * Constructor: __construct
     *           * Allow for CORS, assemble and pre-process the data
     */
    protected $mo_config=null;
    protected $wallet_url;
    public function _construct($request){
        $this->wallet_url = $this->config['fdwallet']['url'];
        $this->log = new Logger ( "/var/log/lotto/lottoFD_info.log" , Logger::DEBUG );
        $this->utils = new sFUtils($this->log, 'F',
                $app=$this->app . $route. "D");
        $this->utils->set_config();
        $this->utils->set_lotto_config();
        $this->utils->config;
        $this->utils->get_lotto_config();
        $this->utils->mo_config();
        $this->utils->get_bulk_config();
        $this->utils->set_config();
        $this->utils->set_lotto_config();
        //$this->db = $this->utils->db;
        //  header("Access-Control-Allow-Orgin: *");
        //  header("Access-Control-Allow-Methods: *");
        //  header("Content-Type: application/json");
        $this->init_db();
        $this->args = explode('/', rtrim($request, '/'));
        $this->log->LogDebug("**********".$this->args);
        $this->endpoint = array_shift($this->args);
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        switch($this->method) {
            case 'DELETE':
            case 'POST':
                $this->request = $this->_cleanInputs($_POST);
                $this->file = file_get_contents("php://input");
                break;
            case 'GET':
                $this->request = $this->_cleanInputs($_GET);
                break;
            case 'PUT':
                $this->request = $this->_cleanInputs($_GET);
                $this->file = file_get_contents("php://input");
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }
    }
    /*
       Main recieves all the requests and forwards them to the relevant function for processing.
       Can also be used to implement middleware eg Authentication/Encryption and decryption.

     */

    public function Main(){
        if(method_exists($this, $this->endpoint)){
            return $this->_response($this->{$this->endpoint}($this->args,$this->db));
        }
        return $this->_response("No Such Endpoint: $this->endpoint", 404);

    } 
    /*
       The following function is used to validate msisdn
     */
    protected function validatemsisdn($msisdn){
        preg_match('/^(\+)?(254|0)?(7\d{8})\s*/', $msisdn, $matches);
        if(!empty($matches)){
            return "254" . $matches[3];
        }
        return null;

    }

    private function _response($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
        return json_encode($data);
    }

    private function _cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }
    private function _requestStatus($code){
        $status = array(
                200 => 'OK',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                500 => 'Internal Server Error',
                );
        return ($status[$code])?$status[$code]:$status[500];
    }
     //initialize db class
     public function init_db(){
       $this->db = new Db($this->log,$this->config['supa4database']);
    }
    // initialize log class

    private function get_log($log){
        return $this->log;
    }
    protected function Mainwithdraw($msisdn, $amount,$source,$pin){
        $payload = array(
                "amount"=>$amount,
                "source"=>$source,
                "destination"=>$msisdn,
                "pin"=>$pin,
                "type"=>"b2c",//debit/b2c
                "request"=>"withdrawal",
                );
        $data_string = json_encode($payload);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Withdraw response: ".var_export($response, true));
        if(is_null($response)){
            return array("balance"=>null,  "success"=>null);
        }
        $withdraw_success = $response['status'] == 200 && $response['message'] == 'OK';
        return array("balance"=>$response['more'][$source]['accountBalance'],
                "success"=>$withdraw_success,
                "charge"=>$response['more'][$source]['accountCharge'],
                "reference"=>$response['more'][$source]['referenceNumber']);

    }
    protected function debit($msisdn, $amount,$pin,$source){
        $this->log->LogDebug("DEBIT REQUEST: msisdn-> ".$msisdn." amount->".$amount." pin-> ".$pin." source-> ".$source);
        $payload = array(
                "amount"=>$amount,
                "source"=>$source,
                "destination"=>$msisdn,
                "pin"=>$pin,
                "type"=>"DEBIT",
                "request"=>"withdrawal",
                );
        $data_string = json_encode($payload);
        $response =  $this->post_to_server($data_string);
        $this->log->LogDebug("Debit response: ".print_r($response, true));
        $debit_success = $response['status'] == 200 && $response['message'] == 'OK';
        $balance = array_key_exists($source, $response['more'])
            ? $response['more'][$source]['accountBalance']:0;

        return array(
                "balance"=>$balance,
                "success"=>$debit_success,
                "fresponse"=>$response);
    }
    private function post_to_server($params){
        $service_url = $this->wallet_url;
        $curl = curl_init($service_url);
        $curl_post_data = $params;
        $this->log->LogDebug("Curl request:".$service_url.",  ".var_export($params, true));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return json_decode($curl_response, true);
    }
    protected function getProfileId($msisdn){
        $profileQuery = "select profile_id from profile where msisdn =$msisdn limit 1";
        $dbresponse = $this->db->select($profileQuery);
        //Create profile if missing
        if(empty($dbresponse)){
            $profileQ = "insert into profile set msisdn =$msisdn,"
                ."user_status=0,"."password='".$hashedPassword."',created_at=now(),updated_at=now()";
            $this->log->LogDebug("Received New Profile creating ...");

            $profile_id = $this->db->query($profileQ, 1);
            $is_new_profile = true;
            $names = null;

        }else{
            $is_new_profile = false;
            $profile_id = $dbresponse[0]['profile_id'];
            $names = $profileQ[0]['name'];

        }
        return $profile_id;
        /*array(
          'profile_id'=>$profile_id,
          'names'=>$names,
          'is_new_profile'=>$is_new_profile
          );*/

    }
    //have int where int is expected
    protected function _int($data){
        if(is_numeric($data) && $data == 0){
            return 0;
        }
        $cleanInt = (int)$data;
        if(!is_int($cleanInt) || $cleanInt == 0){
            return false;
        }else{
            return $cleanInt;
        }

    }
    public function get_balance($msisdn,$pin,$source){
        $payload = array(
                "account"=>$msisdn,
                "pin"=>$pin,
                "request"=>"balance",
                );
        $data_string = json_encode($payload);
        $this->log->LogDebug("BALANCE REQUEST: msisdn-> ".$msisdn."  pin-> ".$pin." source-> ".$source);
        $response =  $this->post_to_server($data_string);
        if($response['status'] ==200){
            $balance = $response['more'][$source]['accountBalance'];
        }else{
            $balance = null;
        }
        $arr_balance = is_null($balance) ?
            array(
                    'balance' => 0,
                    'message' => $response['message'],
                    'status' => $response['status'],
                    'reason' => $response['reason']
                 )
            :   array(
                    'balance' => $balance,
                    'message' => $response['message'],
                    'status' => $response['status']
                    );
        return $arr_balance;
    }
    protected function requestdepo($msisdn,$amount,$paybill,$ref){
        $checkout_url = "http://197.248.6.28/payment_relay/demo.php";
        $this->log->LogDebug("DEBIT REQUEST: msisdn-> ".$msisdn." amount->".$amount." paybill-> ".$paybill." ref-> ".$ref);
        $depodata = [
            'msisdn'=>$msisdn,
            'amount'=>$amount,
            //'callback_url'=>'http://104.155.17.94/lotto/richboxApi/v1/requestdeposit',
            'callback_url'=>'http://104.155.17.94/lotto/supa4/v1/depositrequest',
            'paybill'=>$paybill,
            'reference_id'=>'dosika-'.$ref,
            ];

        $jsondata = json_encode($depodata,TRUE);
        $curl = curl_init($checkout_url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$jsondata);
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $this->log->LogDebug("requestdepo: $json_response");
        if ($status != 200) {
            return false;
        }
        curl_close($curl);

        return true;


    }
    public function depositrequest(){
        $this->log->LogDebug("RECEIVED Deposit REQUEST ". var_export($this->file, 1));
        parse_str($this->file, $requestData);
        $profileID=$this->getProfileId($requestData['msisdn']);

        $this->insertDeposit(
                $profileID,
                $requestData['names'],
                $requestData['amount'],
                $requestData['business_number'],
                $requestData['msisdn'],
                $requestData['reference'],
                $requestData['transaction_id']
                );
        $fname = $requestData['names'];
        $msisdn=$requestData['msisdn'];
        if(! isset($fname)){
            $fname = 'Customer';
        } else{
            $fname = $requestData['names'];
        }
        $sms = "Dear $fname, We have received Ksh. ".$requestData['amount']."Keep playing with DOSIKA to become a WINNER!!, http://dosika.co.ke/.
            For any inquiry call Customer Care on 0729290224 ";
        $outboxID =  $this->outbox($msisdn, $sms, $this->lotto_config['bulk_code'],$profileID);
        $this->send_sms($sms, $outboxID,'1', $msisdn,
                $this->lotto_config['bulk_code'],$this->lotto_config['bulk_service_id'],$this->get_mo_config());
        return [
            'data'=>['description'=>'success'],
            'code'=>200,
            ];
    }
    public function withdrawalrequest(){
        /*
           {'status': u'COMPLETE', 'msisdn': u'254740579427', 'receipt_number': u'MGN0NZV8ZI', 'balance': u'40.00', 'reference': u'EWALLET_5b55e6fca7c39'}
         */
        parse_str($this->file, $requestData);
        $this->log->LogDebug("$this->origin  API:: [ $this->request ]
                DATA:[".json_encode($requestData)." ]");

        $msisdn = $requestData['msisdn'];
        $ref = $requestData['reference'];
        $status = $requestData['status'];
        if($status=='COMPLETE'){
            $dbstatus='SUCCESS';
            $this->log->LogDebug("Withdrawal_STATUS:: ".$dbstatus);
        }else{

            $dbstatus='FAILED';
            $this->log->LogDebug("Withdrawal_STATUS:: ".$dbstatus);
        }
        $this->withdrawStatus($msisdn,$ref,$dbstatus);
        $data = ["status"=>1,"desc"=>"Record updated"];
        return ['data'=>$data,"code"=>200];

    }
}
?>
