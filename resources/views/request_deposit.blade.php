@extends('layouts.app')


@section('content')


    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="heading text-center">Deposit</h4>
                    <form action="{{  route('request_deposit') }}" class="login-form" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input type="text" class="form-control" id="amount" name="amount" value="20">
                        </div>

                        <input type="text" name="msisdn" value="{{ session('msisdn')  }}" class="hidden">

                        @if ($url = Session::get('url'))
                        <input type="hidden" class="form-control" name="url" value="{{ $url }}">
                        @endif

                        <button type="submit" class="btn btn-lg play-btn btn-warning">Deposit</button>
                        <br>
                    </form>

                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif



                </div>
            </div>
        </div>
    </div>

@stop