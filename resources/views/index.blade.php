@extends('layouts.app')


@section('content')

    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="home-banner">
                        <img src="imgs/supa4-04.png" alt="winning banner">
                    </div>
                    <div class="playpicks">
                        <a class="btn btn-warning btn-block" href="/play">
                            Press to pick your 4 lucky numbers
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="text-white">Last Draw Winning Numbers</p>
                    @php include base_path()."/resources/views/winning_numbers.php" @endphp
                    <div class="last-draw">
                        <span class="pick1"> {{ substr($winning_lucky_no,0,2) }} </span>
                        <span class="pick2"> {{ substr($winning_lucky_no,3,2) }}</span>
                        <span class="pick3"> {{ substr($winning_lucky_no,6,2) }}</span>
                        <span class="pick4"> {{ substr($winning_lucky_no,9,2) }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

