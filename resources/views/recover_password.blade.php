@extends('layouts.app')


@section('content')

    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="heading text-center">Recover Password</h4>
                    <form action="{{  route('recover_password') }}" class="login-form" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="msisdn">Phone Number</label>
                            <input type="text" class="form-control" id="msisdn" name="msisdn" >
                        </div>
                        <button type="submit" class="btn btn-block btn-success">Recover</button>
                    </form>

                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>


@stop