<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dosika</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class="wrapper">
        <div class="header">
            <div class="navbar">
              <div class="container-fluid">
                <div class="col-xs-5">
                    <a class="navbar-brand" href="index.php">
                    <img alt="Brand" src="imgs/logo.png">
                  </a>
                </div>
                <div class="col-xs-7">
                    <div class="navbar-right">
                      <a href="login.html" class="btn btn-sm btn-white navbar-btn">Login</a>
                      <a href="register.php" class="btn btn-sm btn-white navbar-btn">Register</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="navbar ">
                <div class="container-fluid">
                    <div class="col-xs-12 sub-menu">
                        <a href="" class="btn btn-xs btn-primary navbar-btn">How to Play</a>
                        <a href="" class="btn btn-xs btn-primary navbar-btn">Deposit</a>
                        <a href="" class="btn btn-xs btn-primary navbar-btn">Winners</a>
                    </div>
                </div>
            </div>


            <div class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="heading text-center">Register</h4>
                            <p id="success_msg" style="color:green;"></p>
                            <p  id="error_msg" style="color:indianred;"></p>
                            <form action="" class="login-form" method="post">
                            <div class="form-group">
                                <label for="phone">Phone Number</label>
                                <input type="text" class="form-control" id="phone" name="msisdn"  placeholder="Phone Number">
                              </div>
                              <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                              </div>
                              <div class="form-group">
                                <label for="passwordConfirm">Confirm Password</label>
                                <input type="password" class="form-control" id="passwordConfirm" name="password2" placeholder="Password">
                              </div>
                              <div class="form-group">
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox"> By Signing up you Agree to <a href="">Our terms and conditions </a>
                                    </label>
                                  </div>
                              </div>
                              <button type="submit"  class="btn btn-default" name="register">Submit</button>




                          </form>


                            <?php
                            include 'net/net.php';


                            $register = $_POST['register'];


                            if(isset($register)) {

                                $password  = @$_POST['password'];
                                $password2  = @$_POST['password2'];
                                $msisdn = @$_POST['msisdn'];

                                if($password === $password2){
                                    $net = new Net();
                                    $response = $net->register($msisdn,$password);
                                    $error = $response['error'];
                                    $message = $response['message'];
                                    $code = $response['code'];
                                    // echo $code;
                                    if($error ==0 && $code == 200 ){
                                        header("Location: verify.php");
                                    }else if($code == 100){
                                        echo '<script>
                        document.getElementById("error_msg").innerHTML = "'.$message.'";
                      </script>';
                                    }

                                }else{
                                    echo "<script>
                        document.getElementById('error_msg').innerHTML = '[Error] : Passwords do not match , please try again!';
                      </script>";
                                }



                            }

                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="nav">
                              <li><a href="#">CONTACTS</a></li>
                              <li><a href="#">FAQS</a></li>
                              <li><a href="#">TERMS AND CONDITIONS</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       </div>
       <!--  -->
       <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 		<script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- <script src="js/main.js"></script> -->


    </body>
</html>