<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Inuka</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-xs-4">
            <a href="index.html" class="logo">
              <img src="imgs/logo.jpg" alt="">
            </a>
          </div>
          <div class="col-xs-8 text-right">
            
            
            <?php
if(isset($_SESSION['isLoggedIn'])) {
?>
 <ul class="menu nav navbar-nav">
              <li><a href="logout.php">log out</a></li>
              <li><a href="play.php">Play</a></li>
           </ul>
<?php
} else{
?>
            <ul class="menu nav navbar-nav">
              <li><a href="login.php">Login</a></li>
              <li><a href="register.php">Register</a></li>
              <li><a href="play.php">How to play</a></li>
            </ul>
<?php
}
?>
          </div>
        </div>
      </div>
    </div>
    <div class="stripe">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h3 class="text-center">Terms and conditions</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="terms">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h3>Inuka Lotto Terms and conditions</h3>
          </div>
        </div>
        <div class="row terms-content">
          <div class="col-xs-12">
            <p>These general Terms and Conditions ("Terms and Conditions" or "T&C") shall be valid for all games offered by Inuka Lotto. By opening an account with Inuka Lotto, and using our services you agree to be bound by the Terms and Conditions.</p>
            <p>Inuka Lotto reserves the right to change these Terms and Conditions at any time and without prior notice and will endeavor to inform you about major modifications to the Terms and Conditions. We may need to change the Terms of Use from time to time for a number of reasons, including (without limitation) for commercial reasons, to comply with law or regulations, to comply with instructions, guidance or recommendations from a regulatory body, or for customer service reasons.</p>
            <p>Inuka Lotto is authorized and regulated by the Betting Control and Licensing Board of Kenya (BCLB) under the Betting, Lotteries and Gaming Act, Cap 131, Laws of Kenya under License number 1202. This Lottery remains subject to the provisions of the Betting Lotteries and Gaming Act (Cap 131 Laws of Kenya) and any disputes arising shall be resolved in accordance with such provision</p>
            <h4 class="my-5">DEFINITIONS</h4>
            <p>These words within these Terms and Conditions shall have the following meanings:-</p>
            <ul>
              <li>
                “Player” shall mean any person who uses the Lottery Service.
              </li>
              <li>
                “Player funds” means money deposited or won amounts available in the player account.
              </li>
              <li>
                “Customer” means any person who buys a lottery ticket in order to participate in the lottery.
              </li>
              <li>
                “Entry” means the answers submitted for purposes of participating in the lottery.
              </li>
              <li>
                “Lottery draw” means the random selection process of the winning participant.
              </li>
              <li>“Draw Cycle” means the 1 hour time frame in which your entry will be eligible for the hourly
              draw</li>
              <li>
                &quot;Account&quot; means the account opened and operated by an Account Holder for the purpose of
                placing Entries via the Account Waging System;
              </li>
              <li>
                &quot;Account Waging System&quot; means any system or method of electronic communication used by
                Inuka Lotto to provide the Question and Answer Game, or used by the Account Holder to
                participate in the Question and Answer Game, including without limitation, the Internet,
                telephone, television, radio or any other kind of electronic or other technology facilitating
                communication in accordance with the Account Opening and Operating Terms and Conditions
                as may be amended by the Company from time to time;
              </li>
              <li>
                &quot;Account Holder&quot; refers to the Participant who is the verified owner provided with an account
                to operate for purposes of placing an Entry or Entries with the Company via the Account Betting
                System;
              </li>
              <li>
                &quot;Claimant&quot; means a person claiming a Prize under the Rules, as the case may be;
              </li>
              <li>
                &quot;Transaction History&quot; refers to up to six (6) months of details of the Account Holder&#39;s
                transactions as recorded in Inuka Lotto&#39;s computer systems
              </li>
              <li>
                “Lottery ticket” means the answer selected by the player.
              </li>
              <li>
                “Kshs” means Kenya Shillings.
              </li>
              <li>
                “Issue” means the same as draw.
              </li>
            </ul>
            <p>Inuka Lotto’s charitable cause is to make a contribution in the fight against Cancer. As part of this Inuka Lotto will fund organizations that are making a difference in the fight against Cancer.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">THE GAME MECHANICS</h4>
            <p>In this Question and Answer Game , an intending Participant has to answer a series of questions in a
              given hour, awarding each correct answer 10 points and 5 points for a wrong answer. Total entries
              drawn or adopted by Inuka Lotto under and in accordance with the Rules shall qualify the Participant for
            a prize or prizes as provided in these Question and Answer Game Rules (General).</p>
            
            <h4 class="my-5"> DRAWS</h4>
            <p>Participants should check the results of the Relevant Draw by referring to the official information released by Inuka Lotto on the Website. Inuka Lotto reserves the right to rectify any errors discovered after the official information has been released on the Inuka Lotto Website, and the results of the Relevant Draw will be the latest rectified version</p>
            <p><strong>The draws will be conducted every hour</strong></p>
            <ul>
              <li>Draws shall be held at such place, on such date, at such time and in such manner as may be
              decided by Inuka Lotto.</li>
              <li>All entries irrespective of time played within that hour will be eligible for the hourly draw.</li>
              <li>Each draw shall have a winning participant and their mobile number shall be disseminated to
              the general public on completion via our website and social media pages.</li>
              <li>If during the course of any draw, any fault or failure shall occur in any associated equipment
                utilised for such draw, the Inuka Lotto may in its absolute discretion declare such number or
                numbers as duly drawn or so much of the draw as at such time been completed as void and
                commence a new draw whether using the same equipment or otherwise. Upon the declaration
              of a draw as void as aforesaid, the same shall be deemed not to have taken place.</li>
            </ul>
            
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">PRIZE STRUCTURE</h4>
            <p>The prizes shall be awarded every hour based on the entries the participant has in that hours’ draw:</p>
            <h4 class="my-3">Alterations and Notification</h4>
            <ul>
              <li>The Company may at its discretion and at any time alter, for any particular draw, the price of the Tickets and/or the prizes offered (including but not limited to offering additional non-monetary prizes) for that draw or the entire prize structure of the Game.</li>
              <li>In the event there are two or more winners for a particular cash prize, the company shall divide the cash prize equally among/between the winning participants.</li>
              <li>Any alterations made pursuant to the above shall, before the Tickets for that particular draw or the draw that is first affected by the change in the prize structure are placed on sale, be published in such medium (such as the Inuka Lotto&#39;s website) as the Inuka Lotto may decide.</li>
              <li>In the event of any alteration in the prizes offered for any draw or draws, the amount, if any, shall, upon such alteration, be dealt with in such manner as Inuka Lotto may decide.</li>
            </ul>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">GENERAL TERMS</h4>
            <p>Inuka Lotto is licensed by BCLB and undertakes to abide by all the applicable laws, regulations and rules. The contract between Inuka Lotto and the customer is made in terms of these laws.</p>
            <p>The customer warrants that they are 18 years old or older, and eligible to participate in a lottery. Inuka Lotto accepts no responsibility for entries placed illegally and the customer warrants that they are legally entitled to participate in Inuka Lotto from the jurisdiction in which they reside. </p>
            <p>Players agree to abide by all legislated requirements, including but not limited to the various national and provincial gambling regulations, ' Inuka Lotto’s rules, polices, terms and conditions.</p>
            <p>Inuka Lotto reserves the right to change these Terms and Conditions at any time without notice to its Customers.  Changes will take effect from the date the updated Terms and Conditions are first posted on the Inuka Lotto website.</p>
            <p>By continuing to use the Inuka Lotto service and website, you will be deemed to have accepted updated Terms and Conditions published on the Inuka Lotto website.  In addition, we will endeavor to alert you to changes by a pop-up screen when you access your online Account, and you may be required to actively accept the new Terms and Conditions before being allowed access to your Account.</p>
            <p>The customer undertakes to notify Inuka Lotto of any and all matters, including any change of personal information that might have a bearing on an approved registration.</p>
            <p>If a customer is no longer able to read, understand, agree and comply with all Inuka Lotto rules, policies and terms and conditions, without exception, they should stop operating their Inuka Lotto account immediately, and request Inuka Lotto to close the account. Failure to do so will indicate that they have acceded to the terms and conditions and the company will not be held liable for any loss sustained by the customer.</p>
            <p>Inuka Lotto makes no guarantees as to the availability of our service, website, games or prices. Inuka Lotto will not entertain any claims, and accepts no liability for losses sustained by a customer’s inability to access Inuka Lotto services or website, due to malfunction, cancellation of accounts, suspension of services or otherwise, regardless of the cause.</p>
            <p>The customer is solely responsible for ensuring that he is familiar with the rules of any entries he or she places. The rules as published by The BCLB shall apply to all entries.</p>
            <p>All content, text, examples, and rules on this website are the property of  Inuka Lotto and any unauthorized use or reproduction is prohibited.</p>
            <p>The customer agrees to abide by the terms and conditions of any offer or competition offered by Inuka Lotto. Inuka Lotto reserves the right to alter or amend the terms and conditions of any offer or competition at any stage and at its absolute discretion, whether or not the customer has already taken any action as a result of an offer or competition, including depositing money to their account or purchasing a ticket. </p>
            <p>Inuka Lotto reserves the right to communicate with customers unless the customer has explicitly requested not to be communicated with.</p>
            <p>The customer warrant that they have read, understood, accepted and will comply with all ' Inuka Lotto’s terms and conditions.</p>
            <p>Money held by Inuka Lotto in a customer account that is deemed to have been inactive for twelve months, with no entries to a draw placed, will be forfeited. Note that a Inuka Lotto customer account is not a standard banking account and it shall not be used to keep money other than the use to purchase tickets.</p>
            <p>Notwithstanding anything contained in these Terms and Conditions, the Customer's winnings shall be subject to the governing regulations in the Republic of Kenya and this shall include but not be limited to applicable taxes and levies.</p>
            <p>Inuka Lotto may use the image and details of a winning Player for purposes of marketing and advertising the brand. </p>
            <p>Any non-enforcement by us of any of our rights under these Terms and Conditions will not constitute a waiver of those rights. Any waiver by us of any of our rights under these Terms and Conditions will not constitute a waiver on any subsequent occasion.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">LIMITATION OF LIABILITY</h4>
            <p>None of the Parties involved in the provision of the Inuka Lotto service, whether Inuka Lotto, its staff, its agent, 3rd party contractors or otherwise shall be liable for any losses or damages (whether direct, incidental, consequential, indirect or punitive damages) resulting from the use and/or abuse of the Inuka Lotto services.</p>
            <p>The customer indemnifies Inuka Lotto against any harm caused due to inaccurate information obtained from our website or employees, including but not limited to sports schedules, locations, prices or results.</p>
            <p>Inuka Lotto and its software provider where applicable shall not be held liable for any loss or damages caused by a break down, malfunction or otherwise in software, hardware, internet communications or any occurrence. </p>
            <p>Inuka Lotto reserves the right to cancel, at its discretion, any entry or transaction arising from such occurrence and without incurring liability of any kind, and at any time suspend the Inuka Lotto service in whole or in part. The Customer represents and warrants that it shall indemnify and hold harmless Inuka Lotto and its licensees, successors, related entities, directors, officers, agents, members and employees and software provider (collectively, the “Inuka Lotto Parties”) from and against any and all direct, general loss or damage, liabilities, losses, interest or penalties of whatsoever nature incurred or sustained by the Inuka Lotto Parties and any costs and expenses (including, but not limited to reasonable attorneys’ fees) from claims or actions against any of the Licensor Parties based upon or from any information, content, materials or communications transmitted, received, displayed, used and/or contained on or through the Inuka Lotto Website and any and all transactions processed thereby; and/or any infringement or improper use by the Customer of the Inuka Lotto Service.</p>
            <p>The Customer warrants and represents that neither the use, possession or availability of the Inuka Lotto Services by the Customer will infringe upon or contravene any laws, regulations, ordinances and/or any other applicable codes and rules of conduct, in the jurisdiction in which they reside. Notwithstanding anything to the contrary, under no circumstances shall Inuka Lotto Parties be liable to one another hereunder for any indirect, incidental, special or consequential damages arising out of or in connection with this agreement, loss of business, profits, revenue, loss or damage arising from loss, damage and/or corruption of any data.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">REGISTRATION</h4>
            <p>In order to utilize the services provided by Inuka Lotto a customer will need to open a Inuka Lotto customer account.</p>
            <p>To Register as a player you must:-</p>
            <ul>
              <li>
                Be an individual (a company or other corporate entity cannot be a Player)
              </li>
              <li>
                Hold a M-PESA account
              </li>
              <li>
                Not be currently suspended as a player
              </li>
              <li>
                Be a resident in a jurisdiction where registration and opening of an account with Inuka Lotto does not contravene any applicable law.
              </li>
            </ul>
            <p>Inuka Lotto reserves the right to reject any registration for a new account, and or cancel an existing account at its sole discretion, and without communication to the customer or applicant.</p>
            <p>When you open an account and every time you make an entry you warrant that:</p>
            <ul>
              <li>
                You are 18 years of age or older; and
              </li>
              <li>
                Your use of the Inuka Lotto and making entries to the lottery via the mobile phone or via this website are restricted to jurisdictions where such is not prohibited by law.
              </li>
              <li>
                Registration is personal to you and you cannot transfer, assign or otherwise dispose of your rights and obligations under these Terms and Conditions.
              </li>
            </ul>
            <p>Customers are only permitted one account and are specifically prohibited from opening multiple accounts in the name of or on behalf of other persons. We reserve the right to void any entries we deem to have been placed by a single customer on multiple accounts.</p>
            <p>The user is responsible in ensuring that they don’t infringe on any laws in their jurisdiction when opening an account and conducting business with Inuka Lotto at all times during the subsistence of their subscription/ participation.</p>
            <p>Inuka Lotto will accept no liability from third parties whatsoever, resulting from you providing incorrect or false data.</p>
            <p>If Inuka Lotto detects accounts which have been setup deliberately with misleading information, have displayed criminal behavior, or if Inuka Lotto concludes that the account-owner has fraudulent intentions, Inuka Lotto shall be entitled to close the accounts and confiscate all funds, including initial deposit.</p>
            <p>Inuka Lotto reserves the right to undertake the following:</p>
            <ul>
              <li>
                Verify your identity
              </li>
              <li>
                Verify your activity and use of all services offered
              </li>
              <li>
                Conduct security and other internal procedures in relation to your account
              </li>
              <li>
                Ensure that prizes offered and award procedures have been adhered to
              </li>
              <li>
                Disclose winners' personal information(name, images etc) on any media platform for marketing purposes
              </li>
            </ul>
            <p>Completion of the registration process implies that you have accepted and understood all of our terms and conditions. If you do not accept each and every term of this agreement, do not logon or open an account.</p>
            <p>Customers warrant that all information set out in the application form is true and correct. Inuka Lotto will not be held liable for relying on the information presented by a Customer.</p>
            <p>We may withhold payment of any nature and void all your winnings if your Registration details are false or misleading.</p>
            <p>If any of your details change after you become a Player (for example your name), you must send us information confirming that change. If your name has changed we may request that you send us a copy of the relevant documentation, for example a marriage certificate or a deed poll.</p>
            <p>Your mobile phone number is your identity. The identity of customers is determined by mobile phone number. We strongly encourage all customers to sign up for identity programs administered by the mobile phone companies. We use mobile phone numbers to uniquely identify customers and their account details. If transactions are made via your phone by someone who is not the registered user, the registered user will remain liable for their transactions. In addition, we cannot carry out account transactions for accounts which are not identical to the phone number. This means that you cannot, for example, ask to be paid out on a different number other than the number you have purchased a lottery ticket with; also you cannot send money to us and request that this is placed in a different account. In addition to your phone number your initial transaction will initiate the creation of a virtual account to which all monies added will be held.
            </p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">ENTRIES TO THE LOTTERY</h4>
            <p>Inuka Lotto will cancel and void all entries and return all money deposited to an account that Inuka Lotto or the authorities deem to have been registered by or on behalf of an individual under the age of 18.</p>
            <p>Inuka Lotto exclusively reserves the right, to limit, reject and or cancel any entries accepted and confirmed by Inuka Lotto at we deem to have been fraudulently or accidentally or erroneously placed (at our discretion).</p>
            <p>Inuka Lotto exclusively reserves the right, to limit, reject and or cancel any entries accepted and confirmed by Inuka Lotto where Inuka Lotto LOTTERY has either negligently, erroneously or otherwise offered or set incorrect information including but not limited to incorrect prices.</p>
            <p>Entries are only valid when accepted in accordance with all of Inuka Lotto rules, polices and terms and conditions.
              The customer indemnifies Inuka Lotto against any loss caused by incorrect credits or debits to his account, including but not limited to, incorrect deposits or winning payouts. The customer authorises Inuka Lotto to debit any amounts which have been incorrectly credited (through error, negligence or otherwise) to the customer account.
            </p>
            <p>Inuka Lotto reserves the right, and at our sole discretion, to choose the product offering made available, including but not limited to: games, and the acceptance or refusal of any entry. Inuka Lotto may make available or terminate offerings at their sole discretion and reserves the right to make any changes without notice to the customer.</p>
            <p>Customers may not place entries by any means not authorised by Inuka Lotto, including but not limited to, robots and website or software manipulation. Inuka Lotto reserves the right to cancel any entries deemed to have been placed by unauthorised means.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4>PURCHASING A TICKET ON THE WEBSITE</h4>
            <p>To place an entry on the Website using the Account Wagering System, the Account Holder must comply
              with the following procedures and any other instructions that are issued by Inuka Lotto on the Website
            from time to time:-</p>
            <p>Log into the Account Wagering System via the Website, and proceed to the Inuka Lotto webpage to
            select the relevant option(s).</p>
            <p>A verification code will be sent to the Account Holder&#39;s registered Kenyan mobile number, which the
            Account Holder will then need to key in for the purpose of authentication.</p>
            <p>The Account Holder shall then select the options made available at that point in time by Inuka Lotto for
            the placement of entries, including but not limited to the online option.</p>
            <p>The Account Holder is allowed to answer as many questions as they can subject to the deposits in their
            accounts.</p>
            <p>To place an entry, the Account Holder shall select/provide the following options/details in accordance
            with the instructions provided on the Inuka Lotto Website:</p>
            <p>The questions answered on which the entry is to be placed or as selected by the Account Holder
            manually;</p>
            <p>
            The Account Holder shall remain on the page where the entry is placed whilst the entry is being processed, and shall wait for confirmation as to whether the entry has been accepted. Once the Account Holder clicks on &quot;Play” as the case may be, he is deemed to have made an offer to Inuka Lotto for the entry option(s) selected.</p>
            <p>
              Inuka Lotto will proceed to deduct the amount of the entry.
              If full deduction of the monies owed for payment of the Entry is not or cannot be completed, Inuka Lotto
              will declare the Entry invalid and reject the Entry. In that event, Inuka Lotto may in its discretion make
            such counter-offer to the Account Holder as it deems fit.</p>
            <p>
              An entry will be deemed valid and accepted only if full payment has been made for the requisite Entry
            for the Relevant Draw.</p>
            <p>
              By clicking on &quot;Play” as the case may be, the Account Holder is assumed to have read, understood and
              agreed to be bound by these Question and Answer Game Rules, the Question and Answer Game Rules
              (General), the Account Opening and Operating Terms and Conditions and the Inuka Lotto’s Website&#39;s
            Terms and Conditions.</p>
            <p>
              Payment for entries placed will be made using funds available in the Account. In the event there are
              insufficient funds in the Account, payment will not be effected and the Account holder has to top up his
            account via Mobile Money Transfer to Paybill no. 290055.</p>
            <h4>PLACING ENTRY VIA MOBILE PHONE</h4>
            <p>To place an Entry using the Mobile phone, the participant must follow these procedures:-</p>
            <ul>
              <li>The participant must first register by sending an SMS with the word “INUKA”, to short code
              number 29155</li>
              <li>The participant will then receive a confirmation text of his/her registration.</li>
              <li>At his/her discretion the participant will TOP up his/her account by sending a minimum of Kshs.
                10 to Mpesa Paybill No. 290055, Account Number type a specific KEYWORD e.g.“INUKA”. The perticipant will receive a confirmation text from INUKA stating the available balance. A question will then be sent to the participant’s number.</li>
              <li>The participant will then send their answer upon which they will receive a confirmation text of
              his/her entry with another question for them to answer.</li>
            </ul>
            <p>For the avoidance of doubt, where the Account waging System provides for the draw hours on which the Question and Answer Game is available for entry and participation in, the hours as notified to the Account Holder shall always refer to the hour available for entry nearest to the date and time;</p>
            <p>If full deduction of the monies owed for payment of the Entry is not or cannot be completed, the Company will declare the Entry invalid and reject the Entry.</p>
            <p>An Entry will be deemed valid and accepted only if full payment has been made for the requisite amount for the Relevant Draw;</p>
            <p>In the event the Entry given by the Account Holder to Inuka Lotto via SMS are regarded to be ambiguous, contradictory or conflicting, Inuka Lotto may either treat these instructions as void, or act upon these instructions based on good faith and on reasonable assumptions as to what such instructions mean. The Account Holder agrees to waive and release Inuka Lotto from any and all claims
              and to indemnify Inuka Lotto against all losses, damages, costs, expenses and liability suffered by Inuka
              Lotto as a result Inuka Lotto regarding the instructions as void, or otherwise acting in the above
            mentioned manner.</p>
            <p>Inuka Lotto is entitled to, at its sole and absolute discretion, verify any instruction received that purport
              to originate from the telephone number, before acting in accordance with such instructions, Inuka Lotto
              reserves its rights not to proceed with any such instructions if such instructions cannot be so verified.
              Inuka Lotto shall not be held liable for any claim, loss or damage arising out of or in connection with
            Inuka Lotto acting in accordance with or not acting in accordance with such instructions.</p>
            <p>Inuka Lotto reserves its rights not to accept any instructions received without giving any reason
              whatsoever. Inuka Lotto shall not be liable for any loss or damage suffered by the Account Holder arising
              out of or in connection with Inuka Lotto accepting or not accepting the instructions from or purportedly
            from the Account Holder.</p>
            <p>Inuka Lotto reserves the right, and may in its discretion (but shall not be bound) to refer to and rely on its contemporaneous recordings (if any) of any telephone conversations with the Account Holder to resolve any disputes with the Account Holder.</p>
            <h4>TRANSACTION RECORDS FOR REMOTE GAMING</h4>
            <p>Inuka Lotto shall make available to the Account Holder, via such communication channels as may be agreed, the Transaction History of his/her entries.</p>
            <p>Details of all valid and successful Entry transactions under the Account as recorded in Inuka Lotto’s
              computer systems will be reflected in the &quot;Transaction History&quot; section of the Account Holder&#39;s &quot;My
              Account&quot; page. The Account Holder should check the Transaction History to ascertain if any Entry placed
            or intended to be placed, has been accepted by Inuka Lotto.</p>
            <p>The Account Holder acknowledges that due to technical limitations, the Transaction History cannot be processed and updated immediately upon the end of the Entry placement made by the Account Holder.
              Any information which the Account Holder obtains from the Transaction History pending the processing
              and updating thereof may therefore not accurately represent the current state of the Account Holder&#39;s
              transactions (and the details thereof) under the Account, and shall in those circumstances, not be
            binding on Inuka Lotto</p>
            <p>The Account Holder who places Entry(s) with the Company through the Account Entry System shall be
              responsible for checking the details in the Transaction History for any errors or omissions therein upon
              the Transaction History being made available to the Account Holder on the Internet and/or any other
              electronic media. Any errors in, omissions from or disagreement with the details in the Transaction
              History must be notified to Inuka Lotto at least sixty (60) minutes (or such other time period as may be
              stipulated by Inuka Lotto from time to time in its sole discretion) before the start of the Relevant Draw,
              failing which the Account Holder shall be deemed to have accepted that the details in the Transaction
              History are true and accurate in all material respects notwithstanding that in those circumstances, the
              Account Holder may or does not have the opportunity to notify the Company of any errors in, omissions
            from or disagreement with the details in the Transaction History.</p>
            <p>The information or data as recorded in the Company&#39;s computer systems on all Entries placed by the Account Holder shall be binding on the Account Holder.</p>
            <h4 class="my-5">DEPOSITS</h4>
            <p>To play, you must make a deposit to your Inuka Lotto account. Deposits to the account may be made by M-PESA.</p>
            <p>Inuka Lotto holds money deposited to your Inuka Lotto account on your behalf until it is used to purchase an entry.</p>
            <p>All winnings are returned to your Inuka Lotto account via M-PESA.
              Inuka Lotto reserves the right to charge transaction fees, including but not limited to an amount equal to any mobile money transaction fees.
            </p>
            <p>You are not permitted to use your Player funds as security or allow a charge to be granted in favour of your account.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">WITHDRAWALS</h4>
            <p>Withdrawals from your Inuka Lotto account will only be made to a mobile money account in your name.</p>
            <p>We can only pay out to Kenyan mobile money accounts.
              Inuka Lotto may withhold payment pending receipt in our bank account of all money paid to Inuka Lotto by you.
            </p>
            <p>Inuka Lotto reserves the right to charge transaction fees, including but not limited to an amount equal to any mobile money transaction fees, if we deem you to have deposited money and not used your account for legitimate lottery purposes.</p>
            <p>Minimum Withdrawal amount is Kshs 50/=</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">REFUND POLICY</h4>
            <p>All money deposited to your Inuka Lotto LOTTERY account is held by Inuka Lotto on your behalf until it is utilized to make a lottery entry.</p>
            <p>Money held in your Inuka Lotto account may be refunded on request, subject to 24 hours’ notice to enable Inuka Lotto to verify that all of the Terms and Conditions have been complied with.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">SECURITY POLICY</h4>
            <p>Inuka Lotto secures all customer information, both online and offline.</p>
            <p>All data is transmitted using SSL encryption.</p>
            <p>The customer is solely responsible for the safekeeping of his login, password, security details and account details. The Customer indemnifies Inuka Lotto against any claims, losses or damages arising from unauthorized use of his or her account. Unauthorised transactions on a customer account will only be cancelled at Inuka Lotto’s discretion.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">CUSTOMER DATA PRIVACY POLICY</h4>
            <p>All information supplied is strictly confidential, and the customer agrees to the Inuka Lotto privacy policy.</p>
            <p>The customer consents to checks being made against appropriate 3rd party databases to verify identity, personal details or credit card information, and any other information supplied by the customer that Inuka Lotto deems necessary to acquire independent verification. The parties with which Inuka Lotto contracts for the verification of information may keep records of all data requests and data submitted to them by Inuka Lotto.</p>
            <p>All customer personal information is stored securely by Inuka Lotto and will not be shared with 3rd parties, other than information required to process 3rd party checks and payments, which will be transmitted securely to  Inuka Lotto processors. Customers acknowledge that Inuka Lotto will be required to supply their personal details and transaction records to The BCLB and any other statutory body that legally requests access to the data. The customer acknowledges that although every endeavour has been made to ensure customer data privacy, Inuka Lotto LOTTERY makes no guarantees, and will to the best of our ability ensure such information supplied by a customer is not made available to any third party except as required to by law. </p>
            <p>
              The customer consents to the recording of all telephone conversations with Inuka Lotto.
            </p>
            <p>Inuka Lotto tracks and stores information about customers’ use of the website, including the IP address of the computer used to make entries. If you require access to information on what data is held about you, this can be obtained by contacting us.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">DELIVERY POLICY</h4>
            <p>A confirmation message will be sent by SMS to the customer’s registered mobile phone when a question
              has been successfully answered. Answers are deemed to have been accepted as soon as they have been
              registered on our systems and a transaction has been processed against the customer’s account,
              regardless of whether or not the confirmation message is received by the customer.
            </p>
            <p>The customer is solely responsible for ensuring that entries are placed accurately. Entries cannot be cancelled by the customer once accepted by us.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">INTELLECTUAL PROPERTY</h4>
            <p>The Customer or any other party shall have no right to copy, in whole or in part, the Inuka Lotto Product, software and website and shall not modify, adapt, translate, reverse engineer, decompile, disassemble, re-distribute, re-sell or create derivative works based on the Inuka Lotto Product.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">DISPUTE RESOLUTION</h4>
            <p>Whenever Inuka Lotto refuses to pay alleged winnings to a customer or a customer refuses to pay an alleged debt to Inuka Lotto for any reason, and Inuka Lotto and the customer are unable to resolve the dispute to the satisfaction of both parties;</p>
            <ul>
              <li>
                Inuka Lotto will inform the customer that the dispute will be referred to the BCLB for resolution, after which Inuka Lotto will, within forty-eight hours, refer the dispute to the BCLB.
              </li>
              <li>
                The customer is not precluded from lodging a complaint directly with the BCLB.
              </li>
              <li>
                The BCLB shall conduct whatever investigation it deems necessary to resolve the dispute and shall serve a written notice on Inuka Lotto and the customer, informing them of the BCLB resolution.
              </li>
              <li>
                The resolution of the BCLB shall become effective on the date when the parties receive a written notice of the resolution.
              </li>
            </ul>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">FRAUDULENT USE </h4>
            <p>You may only use Inuka Lotto, your account and your Player funds in the manner described in these terms and conditions.</p>
            <p>If you suspect your account Player funds have been interfered with you should immediately change your password and contact us.</p>
            <p>Where fraudulent use is detected and your account is suspended the remaining Player funds will, subject to the findings of our investigation and any criminal proceedings, be deposited into your account less any debt due to us and less any funds lost through fraudulent use, and your account will then be closed.</p>
            <p>Where fraudulent use is detected and your Player funds are credited with a Prize before or during your account’s suspension, we will investigate whether the Prize should be forfeited and the amount repaid to us.</p>
            <p>Inuka Lotto reserves the right to claim any Prizes or withdrawals from you where fraudulent use in relation to your account or Player funds has been detected.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">ADDITIONAL INFORMATION</h4>
            <p>A Player cannot claim any Winnings or payout from any games and Inuka Lotto may at its discretion decide about the validity of an entry or a Player's entitlement to winnings or void an entry if</p>
            <ul>
              <li>
                Entries were placed after the relevant deadline or after the start of the relevant draw;
              </li>
              <li>
                Entries were erroneously accepted after the relevant deadline or if the relevant drawing had already begun or taken place;
              </li>
              <li>
                the Entry was placed on behalf of a Player who has provided false information in connection with their Player Account or otherwise in connection with the Website or the services available through it;
              </li>
              <li>
                the Entry was placed on behalf of a Player who has been involved in any fraud-related activities in relation to the placing of the entry, the operation of the Player Account or otherwise in connection with the Website or the services available through it;
              </li>
              <li>
                the Entry was placed on behalf of a Player who was less than 18 years old or was a director, officer, or employee (or their spouse or dependant) of Any games or of one of the Inuka Lotto companies;
              </li>
              <li>
                the player maintains more than one Player Account;
              </li>
              <li>
                in conjunction with any draw, fraud, technical and/or human error or unwarranted influence was found; or
              </li>
              <li>
                the draw is declared by an organiser or co-organiser as being invalid.
              </li>
            </ul>
            <p>Without prejudice to any other provision of these Terms and Conditions, Inuka Lotto and Any games shall not be liable to any person</p>
            <ul>
              <li>
                for any event beyond its reasonable control including without limitation, any act of God, actual or threatened war or other threat or challenge to governmental authority, confiscation, nationalisation, requisition or destruction of or damage to property by or under the order of any Government or public or local authority, strike, lockout, industrial action, fire, flood, drought, tempest, power cut and/or failure or obstruction of any network, broadcasting or telecommunications service;
              </li>
              <li>
                for the failure of, or damage or destruction to, or any errors caused by the computer systems or records or any third party (including, without limitation, the Website), or any aspect of the foregoing;
              </li>
              <li>
                for delays, losses, errors or omissions in or made by the postal or other delivery service or by the banking system;
              </li>
              <li>
                for any other action or event that prevents or hinders the acceptance of an entry;
              </li>
              <li>
                for the refusal to sell an entry to any person or to allow any person to purchase a ticket; or
              </li>
              <li>
                for any losses caused to Players including misuse or unauthorised use of Passwords, money lost by making entries and failure or malfunction of the equipment or technology of the relevant Player or their Internet service provider.
              </li>
            </ul>
            <p>Promotions, and Special Offers</p>
            <ul>
              <li>
                All promotions, or special offers are subject to promotion-specific terms and conditions and any bonus and/or promotion must be used in adherence with such terms and conditions in order to be eligible to receive the bonus or the promotion.
              </li>
              <li>
                Inuka Lotto reserves the right to withdraw any promotion, or special offer at any time. In the event that Inuka Lotto believes You are abusing or attempting to abuse a promotion, or are likely to benefit through abuse or lack of good faith, then Inuka Lotto may, at its sole discretion, deny, withhold or withdraw from the player any promotion, or rescind any policy with respect to the player, either temporarily or permanently, or terminate and/or block the player’s access to Inuka Lotto’s services and products without being under any obligation to pay the player any amount (even winnings generated from the use of such promotion, or special offer). In the event of subsequent errors or omissions in promotional material, or any special offers, Inuka Lotto reserves the right to withdraw, adjust or correct any and all errors and omissions.
              </li>
              <li>
                Inuka Lotto reserves the right to prevent Users registered, logging in or depositing from certain jurisdictions from participating and being eligible for any or all promotions, at the Inuka Lotto’s sole discretion.
              </li>
              <li>
                The provisions of this section apply to all kind of bonuses, promotions and benefits granted to the player in excess of the actual deposit made by the player.
              </li>
            </ul>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">WINNINGS</h4>
            <p>All winnings must be claimed within ninety (90) days of allocation if they go unclaimed for the above mentioned period, the Player will forfeit his winnings.</p>
            <div class="container-fluid">
              <div class="divider"></div>
            </div>
            <h4 class="my-5">BCLB LOTTERY RULES AND THE BOOKMAKER RULES</h4>
            <p>For more information, please view: Betting, Lotteries and Gaming Act Cap 131 Laws of Kenya.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center footnote">
            <p>*By sending the word "Inuka" to 29155, you authorize inuka lotto to send messages to the registered number. Consent is not required as a condition of purchase, Your text is your electronic signature agreeing to these terms and giving electronic writen consent, contact inuka lotto for a free paper of these terms, Msg and data rates may apply. Not all carriers covered. Play responsibly. Must be 18 or older to play.</p>
            <p class="">Copyright &copy; inuka lotto. All rights reserved. By participating in inuka Lotto you agree to the <a href="terms.html"> terms and conditions</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
</body>
</html>
