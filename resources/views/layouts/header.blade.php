<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<div class="wrapper">
    <div class="header">
        <div class="navbar">
            <div class="container-fluid">
                <div class="row">
                <div class="col-xs-5">
                    <a class="navbar-brand" href="/index.php">
                        <img alt="Brand" src="/imgs/logo.png">
                    </a>
                </div>

                    @php
                        $balance = App\Http\Controllers\CustomAuthController::getBalance(session('msisdn'));
                    @endphp
                    @if(session()->has('profile_id'))

                        <div class="col-xs-7">
                            <div class="navbar-right">
                                <!--
                                    <a href="login.html" class="btn btn-sm btn-white navbar-btn">Login</a>
                                    <a href="register.html" class="btn btn-sm btn-white navbar-btn">Register</a>
                                 -->
                                 <div class="flex">
                                     <span class="text-gold">Bal: {{ $balance }}</span>
                                     <small class="text-white text-small">Bonus: 100</small>
                                 </div>
                                

                                <div class="dropdown ml">
                                    <a href="#" class="btn btn-sm btn-white navbar-btn" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Account
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                        <li><a href="#">Deposit</a></li>
                                        <li><a href="/withdraw">Withdraw</a></li>
                                        <li><a href="#">How to play</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="/logout">Logout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @else
                    <div class="col-xs-7">
                        <div class="navbar-right">
                            <a href="/custom-login" class="btn btn-sm btn-white navbar-btn">Login</a>
                            <a href="/custom-register" class="btn btn-sm btn-white navbar-btn">Register</a>
                        </div>
                    </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar ">
                <div class="bg-blue">
                    <div class="container-fluid">
                        <div class="col-xs-12 sub-menu">
                            <p class="text-gold lead">Next Draw<br> <b>TODAY @ 9PM</b>  </p>
                            <div class="">
                                <a href="/" class="text-white ml">Home</a>
                                <a href="/play" class="text-white ml">Play Now</a>
                                <a href="/winners" class="text-white ml">Winners</a>
                            </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>
</div>