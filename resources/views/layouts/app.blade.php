<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dosika</title>
    <meta name="description" content="Dosika lottery, Win upto 100,000 everyday">
    <meta name="keywords" content="Dosika,Win,29066,290066,Lottery,draws">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128416481-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-128416481-1');
    </script>
</head>
<body>


    @include('layouts.header')
            

    <div class="wrapper">
        <div class="notify">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        @include('flash_message')
                    </div>
                </div>
            </div>
        </div>
        @yield('content')



        @include('layouts.footer')
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="{{ asset('bootstrap/bootstrap.min.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        let token = document.head.querySelector('meta[name="csrf-token"]');

        console.log(token);
    </script>

</body>
</html>