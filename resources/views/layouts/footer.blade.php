<div class="foottop">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-4">
                <div class="paybill">
                    <img src="/imgs/paybill.png" alt="paybill image">
                </div>
            </div>
            <div class="col-xs-6 col-sm-8 mt">
                <p class="text-white lead">Match any 3 numbers in sequence to win!! </p>
                <div class="action">
                    @if(session()->has('profile_id'))
                    <a href="/ticket_history" class="btn btn-sm btn-success">Tickets History</a>
                    @endif

                    <a href="/winners" class="btn mt-s btn-sm btn-success">View Winners</a>
                </div>
                

            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav">
                    <li><a href="#">CONTACTS</a></li>
                    <li><a href="#">HOW TO PLAY</a></li>
                    <li><a href="/docs/Dosika-TnC.pdf" download>TERMS AND CONDITIONS </a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
