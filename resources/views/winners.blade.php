@extends('layouts.app')


@section('content')
<h3 class="text-gold">List of Winners</h3>
<div class="table-responsive">
    <table class="table table-striped bg-light">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Draw Time</th>
            <th scope="col">Winning Lucky Numbers</th>
            <th scope="col">Win Amount</th>
            <th scope="col">Phone</th>
        </tr>
        </thead>
        <tbody class="">

            @php $counter = 1;  @endphp
            @foreach($winners as $winner)
            <tr>
            <th scope="row">{{ $counter }}</th>
            <td>{{ $winner->name }}</td>
            <td>{{ $winner->draw_time }}</td>
            <td>{{ $winner->winning_lucky_no }}</td>
            <td>{{ $winner->win_amount }}</td>
            <td>{{ $winner->Phone }}</td>
            </tr>
             @php  $counter += 1;  @endphp
            @endforeach
        </tbody>
    </table>
</div>

@stop