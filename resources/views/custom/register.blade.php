@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="heading text-center">Register</h4>

                    <form action="{{ route('custom.register')  }}" class="login-form" method="post" data-toggle="validator" data-disable="false">
                        @csrf

                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="msisdn"  value="{{ old('msisdn')  }}" placeholder="Phone Number">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"  placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="passwordConfirm">Confirm Password</label>
                            <input type="password" class="form-control" id="passwordConfirm" name="password_confirmation"   placeholder="Password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="checked"> By Signing up you Agree to <a href="">Our terms and conditions </a>
                                </label>
                            </div>
                        </div>

                        <button type="submit"  class="mb-3 btn btn-block btn-success">Register</button>

                    </form>

                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>




@stop