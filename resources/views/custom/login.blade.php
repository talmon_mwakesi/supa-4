@extends('layouts.app')


@section('content')


    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="heading text-center">Login</h4>
                    <form action="{{  route('custom.login') }}" class="login-form" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" class="form-control" id="phone" name="msisdn" placeholder="Phone Number">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
                        </div>
                        @if ($url = Session::get('url'))
                        <input type="hidden" class="form-control" name="url" value="{{ $url }}">
                        @endif
                        <button type="submit" class="btn btn-block btn-success">Login</button>
                        <br>

                        <a class="text-light" href="/forgot_password" style="color: white; margin-top: 50px;"> Forgot Password? </a>
                        <p class="">Don't have an account?  <a class="text-light" href="/custom-register" style="color: white; margin-top: 50px;"> Register Here</a> </p>
                    </form>

                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <p class="alert alert-danger">{{$error}}</p>
                        @endforeach
                    @endif



                </div>
            </div>
        </div>
    </div>

@stop