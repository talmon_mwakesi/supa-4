<?php

 //User plays normally without bonus
 if((!in_array('B', preg_split('/[\s#*]+/', $this->message)))) {

    //let's separate numbers & aletters
    //$numbers = (int) preg_replace('/[\d{2}]/ i','',$this->message);
    $numbers = preg_replace('/[^0-9]/', '', $this->message);
    //confirm play amount as set in configs
    $playamount = $this->lotto_config['play_amount'];
    //numbers picked by the player
    $this->log->LogDebug($this->msisdn . " NUMBERS PICKED: " . $numbers);
    //create an array of said numbers and join them in twos separating them with commas
    $array = str_split($numbers, 2);
    $one = $array[0];
    $two = $array[1];
    $three = $array[2];
    $four = $array[3];
    $choice = $one . "," . $two . "," . $three . "," . $four;
    $this->log->LogDebug("My Choice of Lucky Numbers->>>>" . $choice);
    //if(strlen($numbers) > 8 || strlen($numbers) < 4 || $numbers_check  < 1 || $numbers_check > 48 || empty($numbers) ){
    foreach ($array as $lucky_choice) {
        //Player not subscribed and sends gibberish or tries to play with wrong format
        if ($lucky_choice > 48 || $lucky_choice < 1 || empty($lucky_choice) || strlen($numbers) > 8 || strlen($numbers) < 8) {
            $sms = "Wrong format. Please reply with your 4 lucky numbers between 01 and 48. e.g 14, 02, 34, 08. For Autopick, reply with 1.";

            if (empty($this->link_id) && is_null($this->link_id)) {
                $this->send_message($sms,$this->msisdn);
            sleep(30);
                 if($this->requestDlr($this->msisdn, $this->outbox_id) == 'UserNotSubscribed') {
        $unsubscribed_sms = "Hi, Please reply with the word DOSIKA to enroll to DOSIKA. Wakenya wana SHINDA MA ELFU ya pesa kila Siku!
";
        $this->send_message($unsubscribed_sms, $this->msisdn, 'DOSIKA_BULK');
    }
                if($this->requestDlr($this->msisdn, $this->outbox_id) == 'InsufficientFunds') {
        $this->send_message($sms,$this->msisdn);
    }
       exit;
            }
        }

    }


         //Set $this lotto_cost and $this entry
         $this->entry = $choice;
         $this->lotto_cost = $playamount;

         $this->log->LogDebug("Debiting User account " . $this->msisdn . " with " . $this->lotto_cost . " ". $this->config['wallet']['fdsource']);
         $wallet =  new Wallet($this->log, $this->config['fdwallet']);
         $wallet_debit_array = $wallet->debit($this->msisdn,$this->lotto_cost,'',$this->config['wallet']['fdsource']);
         $this->log->LogDebug(" wallet debit response -- " . var_export($wallet_debit_array['fresponse'], true));
         $wallet_response = $this->process_wallet_response($wallet_debit_array);
         $this->log->LogDebug(" wallet response not debit --- " .
                 var_export($wallet_response, true));
         if (!is_null($wallet_response)) {
             $response_message = $wallet_response;
             //IF SMS MO request send a direct response to phone
             if (empty($this->link_id) && is_null($this->link_id)) {$updatebal = "update outbox set balance = '$userbal' where msisdn = $msisdn  order by outbox_id desc limit 1";
                $this->db->query($updatebal);
                $this->log->LogDebug("Updating Balance ===============>$this->balance for user::$msisdn");
    
    
                $data['entry_id'] = $entryId;
                $this->log->LogDebug("Link ID checking "
                        . $this->link_id . " DATA to QUEUE" . print_r($data, TRUE));
                //PUT the data into the QUEUE   -------
                $jsonData = json_encode($data);
                if ($this->queueMessage($jsonData)) {
    
                } else {
                    $response = "We are unable to process your request at this moment."
                        . " Kindly call our customer care for help: 0729294550.";
                    $this->send_message($response,$this->msisdn, 'DOSIKA_BULK');
                    return $response;
                }
    
            }
    
                 $this->send_message($response_message,$this->msisdn, 'DOSIKA_BULK');
             }
             return $response_message;
         }
         $wallet_debit = $wallet_debit_array['fresponse']['more']
         [$this->config['wallet']['fdsource']]['accountBalance'];
         $wallet_debit_success = $wallet_debit_array['fresponse']['status'] === 200;

         $this->log->LogDebug("Wallet debit response "
                 . $this->msisdn . " : " . $this->lotto_cost . " : "
                 . $wallet_debit . "");
         $this->balance = $wallet_debit;
         $this->log->LogDebug("Wallet Debit OK with balance:" . $this->balance);
         //Updte the Bonus Table
         $profile_idQuery = "select profile_id from profile where msisdn =" . $this->msisdn;
         $pRes = $this->db->select($profile_idQuery);
         $profile_id = $pRes[0]['profile_id'];
         $checkifexists = "select profile_id from bonus where profile_id = $profile_id";
         $checkRes = $this->db->select($checkifexists);
         $ifexists = $checkRes[0]['profile_id'];



         if (empty($ifexists)) {
            //Create the user on bonus
            $bonusAddQuery = " insert into bonus set profile_id = $profile_id, stake = stake + $playamount, bonus_credited=1000, status =1, count = count + 1, unlocked_bonus=0, created_at =now(), updated_at=now();";
            $bonus_id = $this->db->query($bonusAddQuery, 1);
        }
        //Update the user on Bonus
        $updateBnsQuery = "update bonus set stake = stake + $playamount, count = count + 1, updated_at = now() where profile_id = $profile_id limit 1";
        $this->db->query($updateBnsQuery);
        //CHECK IF STAKE >100 UNLOCK BONUS POINT
        $checkUnlockStatus = "select stake, count from bonus where profile_id = $profile_id";
        $Rescheck = $this->db->select($checkUnlockStatus);
        $stake = $Rescheck[0]['stake'];
        $count = $Rescheck[0]['count'];
        $status = 'PRIZE';
        if ($stake >= '100' || $count == '5') {
            $this->log->LogDebug("Checking if STake >= 100" . $stake);
            $unlockBonusQuery = "update bonus set bonus_credited = bonus_credited - 100 , unlocked_bonus = 100, count=0, stake = 0 where profile_id=$profile_id limit 1";
            $this->db->query($unlockBonusQuery);
        }
        $data = [
            'msisdn' => $this->msisdn,
            'profile_id' =>$profile_id,
            'inbox_id' => $this->inbox_id,
            'play_status' => $status,
            'source' => $this->app,
            'lucky_numbers' => $this->entry,
            'stake' => $this->lotto_config['play_amount'],
            ];
        $this->log->LogDebug("PLAY DATA: " . print_r($data, 1));
        $entryId = $this->utils->create_lotto_entry($data);
        $msisdn = $data['msisdn'];
        //Create Ticket
        $ticket_number = $this->utils->create_ticket($msisdn);
        $inbox_id = $data['inbox_id'];
        $profile_id = $data['profile_id'];
        $fnameq = "select name from profile where msisdn = $msisdn";


        $dbRes = $this->db->select($fnameq);
        //return $dbRes[0];
        $names = $dbRes[0]['name'];
        $Bnames = $this->utils->explode_name($names);
        $names_array=explode(' ',$Bnames);
        $fname = $names_array[0];
        if (!isset ($fname)) {
            $fname = 'Customer';
        }
        $sms = "Hi $fname, your numbers:" . $data['lucky_numbers'] ." have been entered in the " . $this->utils->lotto_config['draw_time']." DRAW. Play More, WIN BIG";
       // $outboxID = $this->utils->outbox($msisdn, $sms, $this->utils->lotto_config['bulk_code'], $userbal, $profile_id, $data['reference']);
        //$this->utils->send_sms($sms, $outboxID,$inbox_id,$msisdn, $this->utils->mt_config['bulk_code'],$this->utils->mt_config['bulk_service_id'],$this->utils->get_mtconfig());
        //$this->utils->send_sms($sms, $outboxID, NULL, $msisdn, $this->utils->mt_config['short_code'], $this->utils->mt_config['sdp_service_id'], $this->utils->get_mt_config());
        $this->send_message($sms, $this->msisdn);
        sleep(30);
         if($this->requestDlr($this->msisdn, $this->outbox_id) == 'UserNotSubscribed') {
            $unsubscribed_sms = "Hi, Please reply with the word DOSIKA  to enroll to DOSIKA. Wakenya wana SHINDA MA ELFU ya pesa kila Siku!
";
            $this->send_message($unsubscribed_sms, $this->msisdn, 'DOSIKA_BULK');
        }

         if($this->requestDlr($this->msisdn, $this->outbox_id) == 'InsufficientFunds') {
            $this->send_message($sms,$this->msisdn, 'DOSIKA_BULK');
        }

        //Capture the User Balance after every Entry
        $userbal = $this->balance;
        if (empty($userbal)) {
            $userbal = 0;
        }


        $updatebal = "update outbox set balance = '$userbal' where msisdn = $msisdn  order by outbox_id desc limit 1";
            $this->db->query($updatebal);
            $this->log->LogDebug("Updating Balance ===============>$this->balance for user::$msisdn");


            $data['entry_id'] = $entryId;
            $this->log->LogDebug("Link ID checking "
                    . $this->link_id . " DATA to QUEUE" . print_r($data, TRUE));
            //PUT the data into the QUEUE   -------
            $jsonData = json_encode($data);
            if ($this->queueMessage($jsonData)) {

            } else {
                $response = "We are unable to process your request at this moment."
                    . " Kindly call our customer care for help: 0729294550.";
                $this->send_message($response,$this->msisdn, 'DOSIKA_BULK');
                return $response;
            }

        }
